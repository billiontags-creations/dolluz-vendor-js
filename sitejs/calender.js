var categories = '';
$(function() {

    //load service types
    $.ajax({
        url: listcategories_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            for (var i = 0; i < data.length; i++) {
                $("#li_categories").append(`<optgroup label="${data[i].name}" class="opt${data[i].id}"></optgroup>`);
                for (var j = 0; j < data[i].service.length; j++) {
                    $(".opt" + data[i].id).append(`<option value="${data[i].service[j].id}">${data[i].service[j].name}</option>`);
                }
            }
            categories = $("#li_categories").html().trim();
            data[0].service.length ? listslots(data[0].service[0].id) : '';

        },
        error: function(data) {
            console.log('error occured in service types');
        }
    });

});

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
var service_id = '';

function listslots(serviceid) {
    service_id = serviceid;
    $.ajax({
        url: listservicesslots_api + serviceid,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            $(".li_slots").empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {

                    var created_date = monthNames[parseInt(data[i].created_on.substring(5, 7)) - 1] + " " + data[i].created_on.substring(8, 10) + ", " + data[i].created_on.substring(0, 4);

                    var start_time = data[i].start_time;
                    if (Number(start_time.slice(0, 2)) == 12) {
                        start_time = start_time + ' PM';
                    } else if (Number(start_time.slice(0, 2)) > 12) {
                        start_time = Number(start_time.slice(0, 2)) - 12 + start_time.slice(2, 5) + ' PM';
                    } else {
                        start_time = start_time + ' AM';
                    }

                    var end_time = data[i].end_time;
                    if (Number(end_time.slice(0, 2)) == 12) {
                        end_time = end_time + ' PM';
                    } else if (Number(end_time.slice(0, 2)) > 12) {
                        end_time = Number(end_time.slice(0, 2)) - 12 + end_time.slice(2, 5) + ' PM';
                    } else {
                        end_time = end_time + ' AM';
                    }
                    $(".li_slots").append(` <div class="row slotsection">
                                        <div class="col-md-10 nopadding">
                                            <h5 class="mt-0 mb-5 fw500"><span class="slotname${data[i].id}">${data[i].name}</span> | <span>${created_date}</span></h5>
                                            <p class="slottiming"><span st_time="${data[i].start_time.substring(0,5)}" class="slots_sttime${data[i].id}">${start_time}</span> - <span end_time="${data[i].end_time.substring(0,5)}" class="slots_endtime${data[i].id}">${end_time}</span></p>
                                        </div>
                                        <div class="col-md-2 floatright">
                                            <a class="pointer" onclick="editthis(${data[i].id})" data-toggle="modal" data-target="#editslotsmodal"><i class="ti-pencil"></i></a>
                                        </div>
                                    </div>`)
                }
            } else {
                $(".li_slots").empty().append(`<div class="row slotsection"><div class="col-md-12 nopadding"><center><img src="images/nodata.png" style="width:50%"><p>No Slots Found</p></center></div></div>`);
            }
        },
        error: function(data) {
            console.log('error occured in service types');
        }
    });
}

$("#li_categories").change(function(event) {
    service_id = $("#li_categories").val();
    listslots(service_id);
});

function listslots_submit() {
    service_id = $("#li_categories").val();
    listslots(service_id);
}

var addslots_ip = ['add_slotname', 'add_slottime_1', 'add_slottime_2'];
var addslots_err = ['Slot Name', 'Slot Date', 'Slot Start Time', 'Slot End Time'];

function addslots() {

    for (var i = 0; i < addslots_ip.length; i++) {
        if ($('#' + addslots_ip[i] + '').val() == '') {
            $("#snackbarerror").text("" + addslots_err[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".addslotsBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "service": service_id,
        "name": $("#add_slotname").val(),
        "start_time": $("#add_slottime_1").val() + ":00",
        "end_time": $("#add_slottime_2").val() + ":00"
    });

    $.ajax({
        url: createslots_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".addslotsBtn").html(`Add Slots`).attr("disabled", false);
            service_id = $("#li_categories").val();
            listslots(service_id);
            $("#addslotsmodal").modal("toggle");
            $("#snackbarsuccs").text("Slots Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".addslotsBtn").html(`Add Slots`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

function editthis(slotid){
	sessionStorage.slot_id = slotid;
	$("#edit_slotname").val($(".slotname"+slotid).text());
	$("#edit_slottime_1").val($(".slots_sttime"+slotid).attr("st_time"));
	$("#edit_slottime_2").val($(".slots_endtime"+slotid).attr("end_time"));
}

var editslots_ip = ['edit_slotname', 'edit_slottime_1', 'edit_slottime_2'];

function editslots() {

    for (var i = 0; i < editslots_ip.length; i++) {
        if ($('#' + editslots_ip[i] + '').val() == '') {
            $("#snackbarerror").text("" + addslots_err[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".editslotsBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "service": service_id,
        "name": $("#edit_slotname").val(),
        "start_time": $("#edit_slottime_1").val() + ":00",
        "end_time": $("#edit_slottime_2").val() + ":00"
    });

    $.ajax({
        url: editslots_api + sessionStorage.slot_id + '/',
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".editslotsBtn").html(`Update Slots`).attr("disabled", false);
            service_id = $("#li_categories").val();
            listslots(service_id);
            $("#editslotsmodal").modal("toggle");
            $("#snackbarsuccs").text("Slots Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".editslotsBtn").html(`Update Slots`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

function openslotsmodal(){
    $("#li_categories").val() ? $("#addslotsmodal").modal('show') : '';
}