$(function() {

    $(".changephnoldr").hide();
    
    $("input").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".changephnoBtn").click() : "";
    });

});

//verify otp function starts here
function changephno_fn() {
    if ($('#changedphno').val().trim() == '') {
        $("#snackbarerror").text("Phone Number is required");
        $('#changedphno').addClass("iserr");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $(".changephnoldr").show();
    $(".changephnoBtn").attr("disabled", true);
    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "username": $('#changedphno').val()
    });
    $.ajax({
        url: changephno_api + sessionStorage.registereduserid + '/',
        type: 'put',
        data: postData,
        headers: {
            'content-type': 'application/json',
        },
        success: function(data) {
            $(".changephnoldr").hide();
            $(".changephnoBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".changephnoldr").hide();
            $(".changephnoBtn").attr("disabled", false);
            ajaxerrmsg(data);
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Your Mobile Number Has Been Changed Successfully.Please wait!");
        showsuccesstoast();
        sessionStorage.registeredphoneno = $("#changedphno").val();
        setTimeout(function() {
            window.location.href = "enter-otp.html";
        }, 3000);
    });
} //verify otp function starts here