//success toast fn starts here
function showsuccesstoast() {
    var x = document.getElementById("snackbarsuccs");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//failure toast fn starts here
function showerrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//error toast for ip errors
function showiperrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3500);
}

$(function() {
    localStorage.wutkn ? allow() : '';
    $("body").append('<center><div id="snackbarsuccs"></div><div id="snackbarerror"></div></center>');
    $("#reg_phno,#changedphno,#userphonofp,#rest_phno,#usergivencno,#service_mainphno,.cnt_phno,#add_cont_phno,#edit_cont_phno,#add_sub_phno").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $("#service_zipcode,.price_amnt,.other_pricecombo,#add_other_pricecombo,#other_pricecombo,#acceptamount").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $(".halldetailsrow .ipfield, .halldetailsrow .opipfield").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $(".ipfield").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });

    if (localStorage.wutkn) {
        $(".navbar-top-links").eq(1).empty();
        //top nav bar about admin div
        $(".navbar-top-links").eq(1).html(`<li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="images/logo.png" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">${localStorage.first_name}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="images/logo.png" alt="dolluz" /></div>
                                    <div class="u-text">
                                        <h4>${localStorage.first_name}</h4>
                                        <p class="text-muted">${localStorage.phoneno}</p>
                                        <p class="text-muted useremail_p">${localStorage.emailid == "" ? "N/A" : localStorage.emailid}<span class="emailchecked"></span></p>
                                        <a href="edit-password.html" class="btn btn-rounded btn-danger btn-sm">Edit Password</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a onclick="logout()" class="cptr"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                    `);
        $(".navbar-top-links").eq(1).after(`
        <ul class="nav navbar-top-links navbar-right pull-right">
        <li><a href="subscribe.html"><b>Subscribe</b></a></li>
    </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
        <li><a><b>Call Support: </b><span class="supportno"> +91-9330249330 <span></a></li>
        </ul>
    `);
        $(".top-left-part a").attr("href", "dashboard.html");

        //links in all pages
        $("#side-menu").html(`<li><a href="dashboard.html" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i>Dashboard</a>
                    </li>
                    <li> <a href="calender.html" class="waves-effect"><i class="mdi mdi-calendar-check fa-fw"></i>Calendar</a>
                    </li>
                    <li> <a href="enquiries.html" class="waves-effect"><i class="mdi mdi-clipboard-text fa-fw"></i>Enquiries</a>
                    </li>
                    <li> <a href="leads.html" class="waves-effect"><i class="mdi mdi-account fa-fw"></i>Auto Leads</a>
                    </li>
                    <li> <a href="bookings.html" class="waves-effect"><i class="mdi mdi-account fa-fw"></i>Bookings</a>
                    </li>
                    <li> <a href="manage-subvendors.html" class="waves-effect"><i class="mdi mdi-account fa-fw"></i>Manage Sub-Vendors</a>
                    </li>
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-account-settings-variant fa-fw"></i> <span class="hide-menu">Services<span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">9</span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="javascript:void(0)" class="waves-effect"><i class="fa fa-plus"></i>&nbsp; <span class="hide-menu">Add Service</span><span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li> <a onclick="addservices(1, 'Hall Booking')" class="cptr"><i class="fa-fw">H</i> <span class="hide-menu">Hall Bookings</span></a></li>
                                    <li> <a onclick="addservices(2, 'Caterers')" class="cptr"><i class="fa-fw">C</i> <span class="hide-menu">Caterers</span></a></li>
                                    <li> <a onclick="addservices(3, 'Photographers')" class="cptr"><i class="fa-fw">P</i> <span class="hide-menu">Photographers</span></a></li>
                                    <li> <a onclick="addservices(4, 'Decorators')" class="cptr"><i class="fa-fw">D</i> <span class="hide-menu">Decorators</span></a></li>
                                    <li> <a onclick="addservices(5, 'Dance')" class="cptr"><i class="fa-fw">D</i> <span class="hide-menu">Dance</span></a></li>
                                    <li> <a onclick="addservices(6, 'Light Music')" class="cptr"><i class="fa-fw">L</i> <span class="hide-menu">Light Music</span></a></li>
                                    <li> <a onclick="addservices(7, 'Beauticians')" class="cptr"><i class="fa-fw">B</i> <span class="hide-menu">Beauticians</span></a></li>
                                    <li> <a onclick="addservices(8, 'Resorts and Suite')" class="cptr"><i class="fa-fw">R</i> <span class="hide-menu">Resorts and Suite</span></a></li>
                                    <li> <a onclick="addservices(9, 'Tours and Travels')" class="cptr"><i class="fa-fw">T</i> <span class="hide-menu">Tours and Travels</span></a></li>
                                    <li> <a onclick="addservices(10, 'Freelancers')" class="cptr"><i class="fa-fw">F</i> <span class="hide-menu">Freelancers</span></a></li>
                                </ul>
                            </li>
                            <li><a href="manage-services.html" class="waves-effect"><i class="fa fa-eye"></i>&nbsp; <span class="hide-menu">Manage Service</span></a>
                            </li>
                        </ul>
                    </li>
                    <li> <a href="accounts.html" class="waves-effect"><i class="mdi mdi-note-outline fa-fw"></i>Accounts</a>
                    </li>`);
        $("body").append(`<div id="freelancersmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Select The Freelancers You Want!!!</h4> </div>
                <div class="modal-body">
                    <ul class="tags dynfreelancers">
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>`);
    }
});

//add services page link fn
function addservices(pagetype, name) {
    sessionStorage.serv_subctgry_id = pagetype;
    sessionStorage.serv_created_id = "";
    sessionStorage.service_name = name.toUpperCase(); 
    if (pagetype == 1) {
        window.location.href = "add-hall-service.html";
    } else if (pagetype == 10) {
        loadfreelancers();
    } else {
        window.location.href = "add-service.html";
    }
}

function loadfreelancers() {
    $.ajax({
        url: listfreelancers,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
             $(".dynfreelancers").empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $(".dynfreelancers").append(` <li class="freelancerli" onclick="loadthisfreelancer(${data[i].id}, '${data[i].name}')"> <a class="tag freelanceranchor frlncer${data[i].id} cptr">${data[i].name}</a>
                        </li>`);
                }
            }
            $("#freelancersmodal").modal("toggle"); 
        },
        error: function(data) {
            console.log('error occured in service types');
        }
    });
}

function loadthisfreelancer(freelancerid, name){
    sessionStorage.serv_subctgry_id = freelancerid;
    sessionStorage.service_name = name;     
    $(".dynfreelancers li a").removeClass("frlnactive");
    $(".frlncer"+freelancerid).addClass('frlnactive');
    window.location.href = "add-service.html";    
}


//logout fn starts here
function logout() {
    $.ajax({
        url: logout_api,
        type: 'post',
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            sessionStorage.clear();
            localStorage.clear();

        },
        error: function(data) {
            sessionStorage.clear();
            localStorage.clear();
            window.location.replace("index.html");
        }
    }).done(function(dataJson) {
        window.location.replace("index.html");
    });
} //logout fn starts here

//map show fn starts here
function showmapwindow(lat, long, serviceid) {

    var w = window.open('', '_blank'); //you must use predefined window name here for IE.
    var head = w.document.getElementsByTagName('head')[0];

    //Give some information about the map:
    w.document.head.innerHTML = '<title>Clun Checkin | Map</title></head>';
    w.document.body.innerHTML = '<body><div id="map_canvas" style="display: block; width: 100%; height: 100%; margin: 0; padding: 0;"></div></body>';

    var loadScript = w.document.createElement('script');
    //Link to script that load google maps from hidden elements.
    loadScript.type = "text/javascript";
    loadScript.async = true;
    loadScript.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";

    var googleMapScript = w.document.createElement('script');
    var myLatLng = { lat: 13.033814, lng: 80.246957 };
    //Link to google maps js, use callback=... URL parameter to setup the calling function after google maps load.
    googleMapScript.type = "text/javascript";
    googleMapScript.async = false;
    googleMapScript.text = 'function initialize() {var mapOptions = {center: new google.maps.LatLng(' + lat + ',' + long + '),zoom: 12, mapTypeId: google.maps.MapTypeId.TERRAIN}; var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);var marker = new google.maps.Marker({position: {lat: ' + lat + ', lng: ' + long + '},map: map});}';
    head.appendChild(loadScript);
    head.appendChild(googleMapScript);

} //map show fn ends here


function allow(){
    var p = 0;
    var allowpages = JSON.parse(localStorage.pages);
    if(JSON.parse(localStorage.userdetails).userprofile.role == 3){ return false; }
    for(var i=0; i< allowpages.length ; i++){
        if(window.location.pathname.indexOf(allowpages[i])>-1){
            p++;
        } 
    }
    p === 0 ? window.location.href = "dashboard.html" : '';
}

function getpermissions(dataJson){
    var pages = [];
    var allowpages = ["/calender.html", "/enquiries.html", "/leads.html", "/bookings.html", "-service.html", "/manage-services.html", "/accounts.html"];
    for( var i=0; i< dataJson.permissions.length ; i++){
        pages.push(allowpages[dataJson.permissions[i].permission.id-9]);
    }
    var others = ["/dashboard.html", 
    "/edit-password", "/subscribe.html", "/welcome.html"];
    pages = pages.concat(others);
    localStorage.pages = JSON.stringify(pages);
}

function ajaxerrmsg(data){
    var txt = '';
    for (var key in data.responseJSON) {
       data.responseJSON[key] ? (data.responseJSON[key].non_field_errors ? txt += data.responseJSON[key].non_field_errors[0] + " " : txt = data.responseJSON[key]) : txt = data.responseJSON[key].non_field_errors[0];
    }
    $("#snackbarerror").text(txt);
    showerrtoast();
}