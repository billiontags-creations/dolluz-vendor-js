var add_images = [];
var add_videos = [];
var uploadimagescount = 0;
var uploadvideoscount = 0;
var servicestypes = '';

var listarr_ip = ['seating_capacity', 'floating_capacity', 'dining_capacity', 'indoor_car_capacity', 'outdoor_car_capacity', 'indoor_bike_capacity', 'outdoor_bike_capacity', 'ac_rooms', 'non_ac_rooms', 'veg_breakfast_cost', 'non_veg_breakfast_cost', 'veg_lunch_cost', 'non_veg_lunch_cost', 'veg_dinner_cost', 'non_veg_dinner_cost', 'hour_6_ac_cost', 'hour_6_non_ac_cost', 'hour_12_ac_cost', 'hour_12_non_ac_cost', 'hour_24_ac_cost', 'hour_24_non_ac_cost'];

var listarr_cb = ['is_decoration', 'is_ac_in_hall', 'is_ac_in_dining', 'vessels_available', 'non_veg_allowed', 'outside_food_allowed', 'homam_setup', 'power_backup'];

$(function() {

    if (sessionStorage.pageid == 0) {
        $(".forthefirstpage").show();
        $(".foranother6page").hide();
    } else {
        $(".forthefirstpage").hide();
        $(".foranother6page").show();
    }

    $("#locationcomplete").geocomplete({
        details: ".geo-details",
        detailsAttribute: "data-geo"
    });

    //load service types
    $.ajax({
        url: listservicetypes_api + sessionStorage.categorytype,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            for (var i = 0; i < data.length; i++) {
                servicestypes += `<option value="${data[i].id}">${data[i].name}</option>`;
            }
            $(".add_events_li").empty().append(servicestypes);
        },
        error: function(data) {
            console.log('error occured in service types');
        }
    });

    getservice();

    $(".viewaddedimages").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
    $(".viewaddedvideos").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
    $(".progress").hide();

    //images local Upload fn starts here
    if (window.File && window.FileList && window.FileReader) {

        // addproduct images change fn
        $("#addimages").on("change", function(e) {
            var files = e.target.files;
            if (uploadimagescount == 0) {
                $(".viewaddedimages").html(``);
            }
            for (var i = 0; i < files.length; i++) {
                uploadimagescount++;
                var file = files[i];
                add_images.push(file);
                showpreview(file, uploadimagescount, 1);
            }
        });

        // Reception/Lobby Images change fn
        $("#addvideosid").on("change", function(e) {
            var files = e.target.files;
            if (uploadvideoscount == 0) {
                $(".viewaddedvideos").html(``);
            }
            for (var i = 0; i < files.length; i++) {
                uploadvideoscount++;
                var file = files[i];
                add_videos.push(file);
                showpreview(file, uploadvideoscount, 2);
            }
        });
    } else {
        alert("Your browser doesn't support to File API")
    } //fn ends here

});

//ext local files append fn
function showpreview(f, i, type) {
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            if (type == 1) { // ext images append
                $(".viewaddedimages").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 plr5 addimage" + i + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='" + e.target.result + "' class='img-responsive h85px uploadimg' title='" + file.name + "'/> </div><div class='play'> <a onclick='deletethisimage_local(" + i + ",\"" + file.name + "\"," + 1 + ")' class='bt_pointer'><img src='images/deletebtn.svg' class='w60p'/></a> </div></div></div>");
            } else {
                $(".viewaddedvideos").append("<div class='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mt10 plr5 videoimage" + i + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='images/filepreview.png' class='img-responsive hw100px uploadimg' title='" + file.name + "'/><p class='centered'>" + file.name + "</p> </div><div class='play'> <a onclick='deletethisimage_local(" + i + ",\"" + file.name + "\"," + 2 + ")' class='bt_pointer'><img src='images/deletebtn.svg' class='w60p'/></a> </div></div></div>");
            }
        };
    })(f, i);
    reader.readAsDataURL(f);
}

//delete images locally fn starts here
function deletethisimage_local(i, fileName, type) {
    if (type == 1) { //ext images delete fn
        $('.addimage' + i).remove();
        for (var i = 0; i < add_images.length; i++) {
            if (add_images[i].name === fileName) {
                add_images.splice(i, 1);
                break;
            }
        }
        if (add_images.length == 0) {
            uploadimagescount = 0;
            $(".viewaddedimages").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
        }

    } else { //reception images delete
        $('.videoimage' + i).remove();
        for (var i = 0; i < add_videos.length; i++) {
            if (add_videos[i].name === fileName) {
                add_videos.splice(i, 1);
                break;
            }
        }
        if (add_videos.length == 0) {
            uploadvideoscount = 0;
            $(".viewaddedvideos").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
        }
    }
} //delete files fn starts here


// getservice fn st here
function getservice() {

    $.ajax({
        url: retrievedetails_api + sessionStorage.serviceid + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            //details data
            $("#service_name").val(data.name);
            $("#service_desc").val(data.description);
            $("#service_street").val(data.address.street);
            $("#service_area").val(data.address.area);
            $("#service_city").val(data.address.city);
            $("#service_state").val(data.address.state);
            $("#service_country").val(data.address.country);
            $("#service_zipcode").val(data.address.zipcode);
            $("#service_lat").val(data.address.latitude);
            $("#service_long").val(data.address.longitude);
            $("#service_mainphno").val(data.address.contact_number);
            $("#service_mainland").val(data.address.landmark);

            //cover pic load fn
            if (data.cover_picture) {
                var myElement = document.querySelector(".addbannerimage");
                myElement.style.backgroundImage = "url(" + data.cover_picture + ")";
            }

            //images load
            $(".dyn_existimages").empty();
            if (data.images.length != 0) {
                for (var i = 0; i < data.images.length; i++) {
                    $(".dyn_existimages").append(`<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-xs-12 mt10 plr5 existimage${data.images[i].id} availableimgs">
                                                        <div class="itemsContainer">
                                                            <div class="image widthm10px">
                                                                <img src="${data.images[i].image}" class="img-responsive photosthumb">
                                                            </div>
                                                            <div class="play">
                                                                <a onclick="delete_existimgrvid(${data.images[i].id},1)" data-dismiss="modal"><img src="images/delete-button.svg" class="width85pximg"></a>
                                                            </div>
                                                        </div>
                                                    </div>`)
                }
            }

            //videos load
            $(".dyn_existvideos").empty();
            if (data.videos.length != 0) {
                for (var i = 0; i < data.videos.length; i++) {
                    $(".dyn_existvideos").append(`<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mt10 plr5 existvideo${data.videos[i].id} availablevideos">
                                                        <div class="itemsContainer">
                                                            <div class="image widthm10px"> <img src="images/filepreview.png" class="img-responsive uploadimg" title="Video ${i+1}">
                                                                <p class="centered">Video ${i+1}</p>
                                                            </div>
                                                            <div class="play"> <a onclick="delete_existimgrvid(${data.videos[i].id},2)" class="bt_pointer"><img src="images/deletebtn.svg" class="width85pximg"></a> </div>
                                                        </div>
                                                    </div>`)
                }
            }

            //list services
            $("#li_servicetype").empty().append(servicestypes);
            if (data.types.length != 0) {
                for (var i = 0; i < data.types.length; i++) {
                    $("#li_servicetype option[value='" + data.types[i].type.id + "']").attr("selected", true);
                }
            }

            //list extra detail data
            if (data.hall_details) {
                sessionStorage.hall_details_id = data.hall_details.id;
                for (var i = 0; i < listarr_ip.length; i++) {
                    $(".halldetailsrow .ipfield").eq(i).val(data.hall_details[listarr_ip[i]]);
                }
                for (var i = 0; i < listarr_cb.length; i++) {
                    if (data.hall_details[listarr_cb[i]] == true) {
                        $(".halldetailsrow .cbx").eq(i).attr("checked", true);
                    }
                }
            }

            //contact details 
            $(".contactdetails_li").empty();
            data.contact.length ? '' : $(".contactdetails_li").parent().hide();
            for (var i = 0; i < data.contact.length; i++) {
                $(".contactdetails_li").append(`<tr>
                                            <td class="text-center">${i+1}</td>
                                            <td class="cnt_name_${data.contact[i].id}">${data.contact[i].name}</td>
                                            <td class="cnt_role_${data.contact[i].id}">${data.contact[i].role}</td>
                                            <td class="cnt_phno_${data.contact[i].id}">${data.contact[i].contact_number}</td>
                                            <td class="cnt_email_${data.contact[i].id}">${data.contact[i].email}</td>
                                            <td>
                                                <div class="editbutton"><a class="pointer" onclick="edit_contact(${data.contact[i].id})" data-toggle="modal" data-target="#editcontactdetailsmodal"><i class="fa fa-pencil text-white lh-30"></i></a></div>
                                            </td>
                                        </tr>`)
            }

            // amenities deatils
            $(".amenitiesdetails_li").empty();
            var am_no = 0;
            data.inclusive.length + data.exclusive.length ? '' : $(".amenitiesdetails_li").parent().hide();
            for (var i = 0; i < data.inclusive.length; i++) {
                am_no++;
                $(".amenitiesdetails_li").append(`<tr>
                                                <td class="text-center">${am_no}</td>
                                                <td class="ame_name1${data.inclusive[i].id}">${data.inclusive[i].name}</td>
                                                <td>Inclusive</td>
                                                <td class="ame_desc1${data.inclusive[i].id}">
                                                    ${data.inclusive[i].information}
                                                </td>
                                                <td>
                                                    <div class="editbutton"><a onclick="edit_amenity(${data.inclusive[i].id},1)" class="pointer" data-toggle="modal" data-target="#editamenitiesmodal"><i class="fa fa-pencil text-white lh-30"></i></a></div>
                                                </td>
                                            </tr>`);
            }
            for (var i = 0; i < data.exclusive.length; i++) {
                am_no++;
                $(".amenitiesdetails_li").append(`<tr>
                                                <td class="text-center">${am_no}</td>
                                                <td class="ame_name2${data.exclusive[i].id}">${data.exclusive[i].name}</td>
                                                <td>Exclusive</td>
                                                <td class="ame_desc2${data.exclusive[i].id}">
                                                    ${data.exclusive[i].information}
                                                </td>
                                                <td>
                                                    <div class="editbutton"><a onclick="edit_amenity(${data.exclusive[i].id},2)" class="pointer" data-toggle="modal" data-target="#editamenitiesmodal"><i class="fa fa-pencil text-white lh-30"></i></a></div>
                                                </td>
                                            </tr>`);
            }
            //delivery details 
            if (data.delivery) {
                sessionStorage.delivery_id = data.delivery.id;
                data.delivery.inside_city_available ? $(".cbother13").attr("checked", true) : $(".cbother13").attr("checked", false);
                data.delivery.inside_city_charges ? $(".cbother14").attr("checked", true) : $(".cbother14").attr("checked", false);
                data.delivery.outside_city_available ? $(".cbother15").attr("checked", true) : $(".cbother15").attr("checked", false);
                data.delivery.outside_city_charges ? $(".cbother16").attr("checked", true) : $(".cbother16").attr("checked", false);
                $("#inside_desc").val(data.delivery.inside_city_description);
                $("#outside_desc").val(data.delivery.outside_city_description);
            }
            //other details list
            var odsno = 0;
            $(".otherdetails_li").empty();
            if (data.events.length != 0) {
                sessionStorage.otherdetails_arr = JSON.stringify(data.events);
                for (var i = 0; i < data.events.length; i++) {
                    for (var j = 0; j < data.events[i].others.length; j++) {
                        var fooditems_li = [];
                        for (var k = 0; k < data.events[i].others[j].food_items.length; k++) {
                            fooditems_li.push(data.events[i].others[j].food_items[k].food);
                        }
                        odsno++;
                        $(".otherdetails_li").append(`<tr>
                                            <td class="text-center">${odsno}</td>
                                            <td>${data.events[i].type.name}</td>
                                            <td>${data.events[i].others[j].name}</td>
                                            <td>${data.events[i].others[j].price}</td>
                                            <td>${fooditems_li}</td>
                                            <td>
                                                <div class="editbutton"><a class="pointer" onclick="edit_otherdetails(${i} , ${j}, ${data.events[i].id} , ${data.events[i].others[j].id})" data-toggle="modal" data-target="#editotherdetailsmodal"><i class="fa fa-pencil text-white lh-30"></i></a></div>
                                            </td>
                                        </tr>`)
                    }

                }
            }
        },
        error: function(data) {
            console.log('error occured in notification listing');
        }
    });


} // getservice fn st here

// add details fn starts here
function edit_details_savefn() {
    $(".adddetailssave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = {
        "sub_category": sessionStorage.categorytype,
        "name": $("#service_name").val(),
        "description": $("#service_desc").val(),
        "address": {
            "street": $("#service_street").val(),
            "area": $("#service_area").val(),
            "city": $("#service_city").val(),
            "state": $("#service_state").val(),
            "country": $("#service_country").val(),
            "zipcode": $("#service_zipcode").val(),
            "contact_number": $("#service_mainphno").val()
        }
    };
    $("#service_lat").val()? postData.address["latitude"] = $("#service_lat").val() : '';
    $("#service_long").val() ? postData.address["longitude"] = $("#service_long").val() : '';
    $("#service_mainland").val() ? postData.address["landmark"] = $("#service_mainland").val() : '';
    $.ajax({
        url: edit_details_api + sessionStorage.serviceid + '/',
        type: 'put',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".adddetailssave_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Services Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".adddetailssave_btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//cover pic change fn
function readURL(input, type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            if (type == 1) {
                var myElement = document.querySelector(".addbannerimage");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#coverpic").change(function() {
    readURL(this, 1);
});

function uploadcoverpic() {

    var postData = new FormData();

    if ($('#coverpic')[0].files.length == 0) {
        $("#snackbarerror").text("Cover Picture is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("cover_picture", $('#coverpic')[0].files[0]);
    }

    $(".coverpicsave_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: createservice_cvrpic_api + sessionStorage.serviceid + '/',
        type: 'put',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".coverpicsave_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Cover Picture Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".coverpicsave_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here

}

var deletemethdtype = 0,
    delete_imgrvideo_id = [],
    delete_imgrvideo_id_var = '';

//delete image or video
function delete_existimgrvid(imgrvid_id, type) {
    deletemethdtype = type;
    if (type == 1) {
        if ($(".availableimgs").length != 1) {
            $("#confirmationmodal").modal("toggle");
            delete_imgrvideo_id.push(imgrvid_id);
            delete_imgrvideo_id_var = imgrvid_id;
        } else {
            $("#snackbarerror").text("You don't have permission to delete this image,because atleast one image is required for service");
            showerrtoast();
        }
    } else {
        if ($(".availablevideos").length != 1) {
            $("#confirmationmodal").modal("toggle");
            delete_imgrvideo_id.push(imgrvid_id);
            delete_imgrvideo_id_var = imgrvid_id;
        } else {
            $("#snackbarerror").text("You don't have permission to delete this video,because atleast one video is required for service");
            showerrtoast();
        }
    }
}

function delteimages_videos() {

    var url = deletemethdtype == 1 ? edit_image_delete_api : edit_image_video_api;
    $(".deletimgvid_Btn").html(`Please wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "pk": delete_imgrvideo_id
    });

    $.ajax({
        url: url,
        type: 'DELETE',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            if (deletemethdtype == 1) {
                $(".existimage" + delete_imgrvideo_id_var).remove();
            } else {
                $(".existvideo" + delete_imgrvideo_id_var).remove();
            }
            delete_imgrvideo_id = [];
            $(".deletimgvid_Btn").html(`Submit`).attr("disabled", false);
            $("#confirmationmodal").modal("toggle");
            $("#snackbarsuccs").text("Deleted Successfully!");
            showsuccesstoast();

        },
        error: function(data) {
            $(".deletimgvid_Btn").html(`Submit`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    });
}

function saveimages() {
    var postData = new FormData();

    if (add_images.length == 0) {
        $("#snackbarerror").text("Images is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        for (i = 0; i < add_images.length; i++) {
            postData.append("images", add_images[i]);
        }
    }
    postData.append("service", sessionStorage.serviceid);

    $(".progress").show();
    $('.myprogress').css('width', '0');
    $(".imagesadd_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: createservice_imges_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        // this part is progress bar
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.myprogress').text(percentComplete + '%');
                    $('.myprogress').css('width', percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data) {
            $(".imagesadd_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Images has been updated successfully");
            showsuccesstoast();
            $("#uploadimagesmodal").modal("toggle");
            $('#addimages').val('');
            $('.viewaddedimages').html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
            add_images = [];
            uploadimagescount = 0;
            $(".progress").hide();
            $('.myprogress').css('width', '0');
            getservice();
        },
        error: function(data) {
            $(".imagesadd_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        }
    });
}

function savevideos() {
    var postData = new FormData();


    if (add_videos.length == 0) {
        $("#snackbarerror").text("Videos is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        for (i = 0; i < add_videos.length; i++) {
            postData.append("videos", add_videos[i]);
        }
    }
    postData.append("service", sessionStorage.serviceid);

    $(".progress").show();
    $('.myprogress').css('width', '0');
    $(".videosadd_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: createservice_videos_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        // this part is progress bar
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.myprogress').text(percentComplete + '%');
                    $('.myprogress').css('width', percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data) {
            $(".videosadd_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Videos has been updated successfully");
            showsuccesstoast();
            $("#uploadvideosmodal").modal("toggle");
            $('#addvideos').val('');
            $('.viewaddedvideos').html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
            add_videos = [];
            uploadvideoscount = 0;
            $(".progress").hide();
            $('.myprogress').css('width', '0');
            getservice();
        },
        error: function(data) {
            $(".videosadd_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        }
    });
}

//services types fn add 
function add_servicestype_fn() {

    if ($('#li_servicetype').val() == null) {
        $("#snackbarerror").text("Services Types is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".servicestypesave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var servicetypes = [];
    for (var i = 0; i < $('#li_servicetype').val().length; i++) {
        servicetypes.push($('#li_servicetype').val()[i]);
    }

    var postData = JSON.stringify({ "types": servicetypes });

    $.ajax({
        url: edit_servicetypes_api + sessionStorage.serviceid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".servicestypesave_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Services Types Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".servicestypesave_btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//add extra details fn starts here
var postdata = {};

function add_halldetails_fn() {

    $(".halldetails_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    postdata = {};

    for (var i = 0; i < listarr_ip.length; i++) {
        if ($(".halldetailsrow .ipfield").eq(i).val() != "") {
            postdata[listarr_ip[i]] = $(".halldetailsrow .ipfield").eq(i).val();
        }
    }
    for (var i = 0; i < listarr_cb.length; i++) {
        if ($(".halldetailsrow .cbx").eq(i).val() != "") {
            postdata[listarr_cb[i]] = $(".halldetailsrow .cbx").eq(i).is(":checked") ? true : false;
        }
    }

    var postData = JSON.stringify(postdata);

    $.ajax({
        url: edit_halldetails_api + sessionStorage.hall_details_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".halldetails_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Hall Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".halldetails_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//edit amenities fn starts here
function edit_amenity(id, type) {
    sessionStorage.edit_ametype = type;
    sessionStorage.edit_ameid = id;

    $("#edit_amename").val($(".ame_name" + type + id).text().trim());
    $("#edit_amedesc").val($(".ame_desc" + type + id).text().trim());
}

function edit_amen_fn() {
    if ($('#edit_amename').val() == '') {
        $('#edit_amename').addClass('iserr');
        $("#snackbarerror").text("Inclusive / Exclusive Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_amedesc').val() == '') {
        $('#edit_amedesc').addClass('iserr');
        $("#snackbarerror").text("Inclusive / Exclusive Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".edit_amnBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "is_inclusive": sessionStorage.edit_ametype,
        "name": $('#edit_amename').val(),
        "information": $('#edit_amedesc').val()
    });

    $.ajax({
        url: edit_amenities_api + sessionStorage.edit_ameid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_amnBtn").html(`Save`).attr("disabled", false);
            $(".ame_name" + sessionStorage.edit_ametype + sessionStorage.edit_ameid).text($('#edit_amename').val());
            $(".ame_desc" + sessionStorage.edit_ametype + sessionStorage.edit_ameid).text($('#edit_amedesc').val());
            $("#editamenitiesmodal").modal("toggle");
            $("#snackbarsuccs").text("Amenities Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".edit_amnBtn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//ADD AMENITIES FN STARTS HERE
function add_amenities_fn() {

    if ($('#inclusive_name').val() == '') {
        $('#inclusive_name').addClass('iserr');
        $("#snackbarerror").text("Inclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#inclusive_desc').val() == '') {
        $('#inclusive_desc').addClass('iserr');
        $("#snackbarerror").text("Inclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#exclusive_name').val() == '') {
        $('#exclusive_name').addClass('iserr');
        $("#snackbarerror").text("Exclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#exclusive_desc').val() == '') {
        $('#exclusive_desc').addClass('iserr');
        $("#snackbarerror").text("Exclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".amenities_save_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var inclusives = [{ "name": $("#inclusive_name").val(), "information": $("#inclusive_desc").val() }];
    var exclusives = [{ "name": $("#exclusive_name").val(), "information": $("#exclusive_desc").val() }];

    var postData = JSON.stringify({
        "service": sessionStorage.serviceid,
        "inclusive": inclusives,
        "exclusive": exclusives
    });

    $.ajax({
        url: createamenities_details_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".amenities_save_btn").html(`Save`).attr("disabled", false);
            getservice();
            $("#addamenitiesmodal").modal("toggle");
            $("#snackbarsuccs").text("Amenities Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".amenities_save_btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here

} //ADD AMENITIES FN ENDS HERE

//edit amenities fn starts here
function edit_contact(id) {
    sessionStorage.edit_contactid = id;
    $("#edit_cont_name").val($(".cnt_name_" + id).text().trim());
    $("#edit_cont_email").val($(".cnt_email_" + id).text().trim());
    $("#edit_cont_role").val($(".cnt_role_" + id).text().trim());
    $("#edit_cont_phno").val($(".cnt_phno_" + id).text().trim());
}

function edit_contactdetails() {
    if ($('#edit_cont_name').val() == '') {
        $('#edit_cont_name').addClass('iserr');
        $("#snackbarerror").text("Contact Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_cont_role').val() == '') {
        $('#edit_cont_role').addClass('iserr');
        $("#snackbarerror").text("Contact Role is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_cont_phno').val() == '') {
        $('#edit_cont_phno').addClass('iserr');
        $("#snackbarerror").text("Contact Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_cont_email').val() == '') {
        $('#edit_cont_email').addClass('iserr');
        $("#snackbarerror").text("Contact Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#edit_cont_email').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#edit_cont_email').addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".edit_cntBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "service": sessionStorage.serviceid,
        "name": $('#edit_cont_name').val(),
        "role": $('#edit_cont_role').val(),
        "contact_number": $('#edit_cont_phno').val(),
        "email": $('#edit_cont_email').val()
    });

    $.ajax({
        url: edit_conatctdetails_api + sessionStorage.edit_contactid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_cntBtn").html(`Save`).attr("disabled", false);

            $(".cnt_name_" + sessionStorage.edit_contactid).text($('#edit_cont_name').val());
            $(".cnt_role_" + sessionStorage.edit_contactid).text($('#edit_cont_role').val());
            $(".cnt_phno_" + sessionStorage.edit_contactid).text($('#edit_cont_phno').val());
            $(".cnt_email_" + sessionStorage.edit_contactid).text($('#edit_cont_email').val());

            $("#editcontactdetailsmodal").modal("toggle");
            $("#snackbarsuccs").text("Contact Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".edit_cntBtn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//add contact details fn 
function add_contactdetails() {
    if ($('#add_cont_name').val() == '') {
        $('#add_cont_name').addClass('iserr');
        $("#snackbarerror").text("Contact Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_cont_role').val() == '') {
        $('#add_cont_role').addClass('iserr');
        $("#snackbarerror").text("Contact Role is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_cont_phno').val() == '') {
        $('#add_cont_phno').addClass('iserr');
        $("#snackbarerror").text("Contact Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_cont_email').val() == '') {
        $('#add_cont_email').addClass('iserr');
        $("#snackbarerror").text("Contact Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#add_cont_email').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#add_cont_email').addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".add_cntBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var contactdetails = [{
        "service": sessionStorage.serviceid,
        "name": $('#add_cont_name').val(),
        "role": $('#add_cont_role').val(),
        "contact_number": $('#add_cont_phno').val(),
        "email": $('#add_cont_email').val()
    }];
    var postData = JSON.stringify(contactdetails);

    $.ajax({
        url: create_contactdetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_cntBtn").html(`Save`).attr("disabled", false);
            $("#addcontactdetailsmodal").modal("toggle");
            getservice();
            $("#snackbarsuccs").text("Contact Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".add_cntBtn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//delev details fn starts here
function add_deldetails_fn() {

    if ($('#inside_desc').val() == '') {
        $('#inside_desc').addClass('iserr');
        $("#snackbarerror").text("Inside City Delivery Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#outside_desc').val() == '') {
        $('#outside_desc').addClass('iserr');
        $("#snackbarerror").text("Outside City Delivery Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".add_deldetails_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "inside_city_available": $(".cbother13").is(":checked") ? true : false,
        "inside_city_charges": $(".cbother14").is(":checked") ? true : false,
        "outside_city_available": $(".cbother15").is(":checked") ? true : false,
        "outside_city_charges": $(".cbother16").is(":checked") ? true : false,
        "inside_city_description": $('#inside_desc').val(),
        "outside_city_description": $('#outside_desc').val()
    });
    $.ajax({
        url: edit_deldetails_api + sessionStorage.delivery_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_deldetails_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Other Delivery Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".add_deldetails_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

var delete_fooditems = [];

function edit_otherdetails(ivalue, jvalue, eventsid, detailsid) {
    sessionStorage.edit_otherdetails_id = detailsid;
    var data = JSON.parse(sessionStorage.otherdetails_arr);

    $(".events_li").empty().append(servicestypes);
    $(".events_li option[value='" + data[ivalue].type.id + "']").attr("selected", true);
    $("#other_name").val(data[ivalue].others[jvalue].name);
    $("#other_pricecombo").val(data[ivalue].others[jvalue].price);

    $(".bootstrap-tagsinput").eq(0).find(".label-info").remove();
    $(".edititems").val("");

    delete_fooditems = [];
    $(".dyn_fooditems").empty();
    for (var i = 0; i < data[ivalue].others[jvalue].food_items.length; i++) {
        $(".dyn_fooditems").append(`<div class="col-md-3">
                                <p class="fooditemtags fooditemtags${data[ivalue].others[jvalue].food_items[i].id}">${data[ivalue].others[jvalue].food_items[i].food} &nbsp;&nbsp;<span><img src="images/deletesmall.png" class="delimgintags" onclick="deletethisfood(${data[ivalue].others[jvalue].food_items[i].id})"></span></p>
                            </div>`)
    }
}

function deletethisfood(foodid) {
    if ($(".fooditemtags").length > 1) {
        if (delete_fooditems.includes(foodid)) {
            var i = delete_fooditems.indexOf(foodid);
            if (i != -1) {
                delete_fooditems.splice(i, 1);
            }
        } else {
            delete_fooditems.push(foodid);
        }
        $(".fooditemtags" + foodid).toggleClass('active');
    }
}

function edit_otherdetails_fn() {

    if ($('#other_name').val() == '') {
        $('#other_name').addClass('iserr');
        $("#snackbarerror").text("Name of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#other_pricecombo').val() == '') {
        $('#other_pricecombo').addClass('iserr');
        $("#snackbarerror").text("Price of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    // if ($('#edititem').val() == '') {
    //     $("#snackbarerror").text("Add Items is Required");
    //     showerrtoast();
    //     event.preventDefault();
    //     return;
    // }
    $(".otherdetails_editsave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var fooditems = [];
    if ($(".edititems").val() != "")
        for (var j = 0; j < $(".edititems").val().split(",").length; j++) {
            fooditems.push({ "food": $(".edititems").val().split(",")[j] })
        }

    var postData = JSON.stringify({
        "event": $(".events_li").val(),
        "name": $("#other_name").val(),
        "price": $("#other_pricecombo").val(),
        "created": fooditems,
        "deleted": delete_fooditems
    });

    $.ajax({
        url: edit_otherdetails_api + sessionStorage.edit_otherdetails_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".otherdetails_editsave_btn").html(`Save`).attr("disabled", false);
            $("#editotherdetailsmodal").modal("toggle");
            getservice();
            $("#snackbarsuccs").text("Other Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".otherdetails_editsave_btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here

}

function add_otherdetails_fn() {

    if ($('#add_other_name').val() == '') {
        $('#add_other_name').addClass('iserr');
        $("#snackbarerror").text("Name of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_other_pricecombo').val() == '') {
        $('#add_other_pricecombo').addClass('iserr');
        $("#snackbarerror").text("Price of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#additem').val() == '') {
        $("#snackbarerror").text("Add Items is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".otherdetails_save_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var fooditems = [];
    for (var j = 0; j < $(".additems").val().split(",").length; j++) {
        fooditems.push({ "food": $(".additems").val().split(",")[j] })
    }

    var postData = JSON.stringify({
        "service": sessionStorage.serviceid,
        "events": [{
            "event": $(".add_events_li").val(),
            "name": $("#add_other_name").val(),
            "price": $("#add_other_pricecombo").val(),
            "food_items": fooditems
        }]
    });

    $.ajax({
        url: create_otherdetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".otherdetails_save_btn").html(`Save`).attr("disabled", false);
            $("#addotherdetailsmodal").modal("toggle");
            getservice();
            $("#snackbarsuccs").text("Other Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".otherdetails_save_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here
}

//final submit fn starts here
function finalsubmit() {

    $(".finalsubmit_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    $.ajax({
        url: create_finalsubmit_api + sessionStorage.serviceid + '/',
        type: 'put',
        headers: {
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".finalsubmit_Btn").html(`Submit`).attr("disabled", false);
            $("#snackbarsuccs").text("Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".finalsubmit_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here
}