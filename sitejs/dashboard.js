$(function() {
    // pushify.optin(function(subscriber_id){
    //     // Store it to your database
    //    pushifySubscribe(subscriber_id);
    // });
    dashboardfunc();
});

function dashboardfunc() {
    $.ajax({
        url: dashboard_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            if(!data.email_updated){
                $("#useremailmodal").modal("toggle");
            }

            if(data.is_email_verified){
                $(".emailchecked").html(`<img src="images/checked.svg" class="emailverified">`);
            } else {
                $(".emailchecked").html(`<img src="images/notchecked.svg" class="emailverified">`);
            }

            $('.totalleadvalue').text(data.leads);
            $('.totalleadvalueprog').css("width", data.leads + '%');

            $('.totalbookingsvalue').text(data.bookings);
            $('.totalbookingsvalueprog').css("width", data.bookings + '%');

            $('.totalenquiriesvalue').text(data.enquiries);
            $('.totalenquiriesvalueprog').css("width", data.enquiries + '%');

            $('.totalbookmon').text(data.bookings_this_month);
            $('.totalbookmonprog').css("width", data.bookings_this_month + '%');

            var chart1 = [{
                "country": "Jan",
                "visits": 0,
                "color": "#FF0F00"
            }, {
                "country": "Feb",
                "visits": 0,
                "color": "#FF6600"
            }, {
                "country": "Mar",
                "visits": 0,
                "color": "#FF9E01"
            }, {
                "country": "Apr",
                "visits": 0,
                "color": "#FCD202"
            }, {
                "country": "May",
                "visits": 0,
                "color": "#F8FF01"
            }, {
                "country": "Jun",
                "visits": 0,
                "color": "#B0DE09"
            }, {
                "country": "Jul",
                "visits": 0,
                "color": "#04D215"
            }, {
                "country": "Aug",
                "visits": 0,
                "color": "#0D8ECF"
            }, {
                "country": "Sep",
                "visits": 0,
                "color": "#0D52D1"
            }, {
                "country": "Oct",
                "visits": 0,
                "color": "#2A0CD0"
            }, {
                "country": "Nov",
                "visits": 0,
                "color": "#8A0CCF"
            }, {
                "country": "Dec",
                "visits": 0,
                "color": "#CD0D74"
            }];

            var chart2 = [{
                "country": "Sunday",
                "value": 0
            }, {
                "country": "Monday",
                "value": 0
            }, {
                "country": "Tuesday",
                "value": 0
            }, {
                "country": "Wednesday",
                "value": 0
            }, {
                "country": "Thursday",
                "value": 0
            }, {
                "country": "Friday",
                "value": 0
            }, {
                "country": "Saturday",
                "value": 0
            }];

            for (var j = 0; j < data.bookings_this_year.length; j++) {
                chart1[data.bookings_this_year[j].month - 1].visits = data.bookings_this_year[j].count;
            }

            for (var i = 0; i < data.booking_this_week.length; i++) {
                chart2[data.booking_this_week[i].weekday - 1].value = data.booking_this_week[i].count / data.total_bookings_this_week;
            }

            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "none",
                "marginRight": 70,
                "dataProvider": chart1,
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Monthly Status"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "<b>[[category]]: [[value]]</b>",
                    "fillColorsField": "color",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "country",
                "categoryAxis": {
                    "gridPosition": "start",
                    "labelRotation": 45
                },
                "export": {
                    "enabled": true
                }
            });

            var chart = AmCharts.makeChart("chartdiv1", {
                "type": "pie",
                "theme": "light",
                "dataProvider": chart2,
                "valueField": "value",
                "titleField": "country",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "angle": 30,
                "export": {
                    "enabled": true
                }
            });

        },
        error: function(data) {
            ajaxerrmsg(data);
        }
    });
}

function update_useremailfn(){
    if ($('#update_useremail').val() == 0) {
        $("#snackbarerror").text("Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#update_useremail').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".updateemail_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = {
        "email" : $("#update_useremail").val() 
    }

    $.ajax({
        url: updateuseremail_api,
        type: 'put',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".updateemail_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("User Email Has Been Updated Successfully!");
            showsuccesstoast();
            localStorage.emailid = $("#update_useremail").val();
            $("#useremailmodal").modal("toggle");
            $(".useremail_p").html($("#update_useremail").val() + `<span class="emailchecked"><img src="images/notchecked.svg" class="emailverified"></span>`)
        },
        error: function(data) {
            $(".updateemail_Btn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

function pushifySubscribe(id){
    $.ajax({
        url: pushifytoken_api,
        type: 'put',
        headers:{
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        data: JSON.stringify({subscriber_id: id}),
        success: function(data){
            console.log(data);
        },
        error: function(data){
            console.log(data);
        }
    })
}