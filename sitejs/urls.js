// var domain = "http://192.168.1.15:8000/";
var domain = "http://dolluz.zordec.com/";

//dashborad api
var dashboard_api = domain + 'super-admin/vendor/dashboard/';

//auth api
var register_api = domain + 'auth/register/';
var verifyotp_api = domain + 'auth/activate/account/';
var changephno_api = domain + 'auth/update/mobile-number/';
var login_api = domain + 'auth/login/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var changepassword_api = domain + 'auth/change-password/';
var logout_api = domain + 'auth/logout/';

//add services apis 
var createservice_details_api = domain + 'services/create/details/';
var createservice_cvrpic_api = domain + 'services/update/cover-picture/';
var createservice_imges_api = domain + 'services/create/images/';
var createservice_videos_api = domain + 'services/create/videos/';

var createamenities_details_api = domain + 'services/create/amenities/';
var listservicetypes_api = domain + 'category/list/create/type/?sub_category=';
var createservicestypes_api = domain + 'services/create/service-types/';
var create_contactdetails_api = domain + 'services/create/contact-details/';
var create_halldetails_api = domain + 'services/create/hall-details/';
var create_delevrydetails_api = domain + 'services/create/delivery-details/';
var create_otherdetails_api = domain + 'services/create/other-details/';
var create_finalsubmit_api = domain + 'services/activate/';

var listfreelancers = domain + 'category/list/create/sub-category/?category=10';

//manage services api
var listing_services_api = domain + 'services/manage/';

//edit service details api
var retrievedetails_api = domain + 'services/retrieve/';

var edit_details_api = domain + 'services/update/details/';
var edit_image_delete_api = domain + 'services/delete/images/';
var edit_video_delete_api = domain + 'services/delete/videos/';
var edit_servicetypes_api = domain + 'services/update/service-types/';
var edit_halldetails_api = domain + 'services/update/hall-details/';
var edit_amenities_api = domain + 'services/update/amenities/';

var edit_conatctdetails_api = domain + 'services/update/contact-details/';
var edit_deldetails_api = domain + 'services/update/delivery-details/';
var edit_otherdetails_api = domain + 'services/update/event-details/';

//create sub vendor page api 
var create_subvendor_api = domain + 'super-admin/create/sub-vendor/';
var list_subvendors_api = domain + 'super-admin/list/sub-vendors/';
var accrdec_user_api = domain + 'super-admin/activate-user/';
var update_userpassword_api = domain + 'super-admin/reset-password/';

//ACCOUNTS PAGE API
var listdetails_api = domain + 'accounts/list/clearance-history/?user=';
var getdeatilsmodal_api = domain  + 'accounts/list/last-clearance/';

//ENQUIRIES,BOOKINGS,LEADS API
var loaddetailsebl_api = domain + 'bookings/list/service-bookings/';
var loadstatistics_api = domain + 'bookings/list/statistics/?status=';
var viewphoneemail_api = domain + 'bookings/update/service-bookings/';
var acceptbooking_api = domain + 'bookings/update/service-bookings/';

// calender page api
var listcategories_api = domain + 'slots/list/services/drop-down/';
var listservicesslots_api = domain + 'slots/list/create/?service=';
var createslots_api = domain + 'slots/list/create/';
var editslots_api = domain + 'slots/update/';

//packages page api
var listpackages_api = domain + 'packages/list/create/';
var pay_api = domain + 'payments/payment-gateway/';
var paymentsuccess_api = domain + 'payments/payment-success/';

//update email 
var updateuseremail_api = domain + 'auth/verify/vendor-email/';
var emailverify_api = domain + 'auth/verify/account/';

//permissions
var listpermissions_api = domain + "super-admin/list/user-permissions/";
var updatepermissions_api = domain + "super-admin/update/permissions/";

var createoffers_api = domain + "offers/create/";
var retrieveoffer_api = domain + "offers/retrieve/"; 
var activateoffer_api = domain + "offers/update/";

var pushifytoken_api = domain + "auth/update/pushify-token/";