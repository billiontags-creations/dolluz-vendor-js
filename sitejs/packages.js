$(function() {
    listpackages();
});

function listpackages() {
    $.ajax({
        url: listpackages_api,
        type: "get",
        headers: {
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".listpackage").empty();
            for (var i = 0; i < data.length; i++) {
                $(".listpackage").append(`
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <ul class="pkg_listing  pkg_card">
                            <li>
                                <h4><b>${data[i].name}</b></h4></li>
                            <li>
                                <h2><b>Rs ${data[i].amount}</b></h2></li>
                            <li class="list_features${i}">
                                
                            </li>
                            <li>
                                <h4>${data[i].count}</h4></li>
                            <li>
                                <button class="btn btn-success pkgbtn paybtn" onclick="pay(${data[i].id})">PAY</button>
                            </li>
                        </ul>
                    </div>
                `);
                for (var j = 0; j < data[i].features.length; j++) {
                    $(".list_features" + i).append(`<div class="divpkg">${data[i].features[j].description}</div>`);
                }
            }
        },
        error: function(data) {
            console.log("error occured in listing packages");
            ajaxerrmsg(data);
        }
    });
}

function pay(id) {
    $.ajax({
        url: pay_api,
        type: "post",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({ package: id }),
        success: function(data) {
            console.log(data);
            //razor pay fn starts here
            var options = {
                "key": "rzp_test_twhGB9MZHnXYT0",
                "amount": data.amount,
                "name": "Dolluz",
                "description": "Payment",
                "image": "http://dolluzvendor-js.billioncart.com/images/logo.png",
                "order_id": data.order_id,
                handler: function(response) {
                    // console.log(response);
                    console.log(response.razorpay_payment_id);
                    console.log(response.razorpay_order_id);
                    console.log(response.razorpay_signature);

                    var postData = JSON.stringify({
                        "razorpay_payment_id": response.razorpay_payment_id,
                        "razorpay_order_id": response.razorpay_order_id,
                        "razorpay_signature": response.razorpay_signature
                    });

                    $.ajax({
                        url: paymentsuccess_api,
                        type: 'POST',
                        data: postData,
                        headers: {
                            "content-type": 'application/json',
                            "Authorization": "Token " + localStorage.wutkn
                        },
                        success: function(pdata) {
                            $("#snackbarsuccs").text("Payment Success");
                            showsuccesstoast();
                            window.location.href = "leads.html";
                        },
                        error: function(pdata) {
                            console.log("Error Occured in payment after razorpay");
                            var errtext = "";
                            for (var key in JSON.parse(pdata.responseText)) {
                                errtext = JSON.parse(pdata.responseText)[key][0];
                            }
                            $("#snackbarerror").text('Payment failure');
                            showerrtoast();
                        }
                    });
                },
                "prefill": {
                    "uid": JSON.parse(localStorage.userdetails).id,
                    "contact": JSON.parse(localStorage.userdetails).username,
                    "name": JSON.parse(localStorage.userdetails).first_name,
                    "email": JSON.parse(localStorage.userdetails).email
                },
                "notes": {
                    "address": "Car parlour Payment"
                },
                "theme": {
                    "color": "#0058ae"
                }
            };

            rzp1 = new Razorpay(options);

            rzp1.open();
        },
        error: function(data) {
            console.log(data);
            ajaxerrmsg(data);
        }
    });
}