$(function() {

    $(".nodatarow").hide();
    sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    listdata(1);
    $("#searchdate").val("");

});


function add_subvendor() {
    if ($('#add_sub_name').val().trim() == '') {
        $('#add_sub_name').addClass("iserr");
        $("#snackbarerror").text("Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_sub_email').val() == '') {
        $('#add_sub_email').addClass('iserr');
        $("#snackbarerror").text("Contact Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#add_sub_email').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#add_sub_email').addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#add_sub_phno').val().trim() == '') {
        $('#add_sub_phno').addClass("iserr");
        $("#snackbarerror").text("Phone Number is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_sub_pwd').val().trim() == '') {
        $('#add_sub_pwd').addClass("iserr");
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_sub_pwd').val().length < 6) {
        $('#add_sub_pwd').addClass("iserr");
        $("#snackbarerror").text("Min 6 Characters is Required for Password!");
        showerrtoast();
        event.preventDefault();
        return;
    }

    var perm = [];
    var $checkip = $(".addpermision input:checked");
    if($checkip.length == 0){
        $("#snackbarerror").text("Permissions is required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    for(var i=0;i< $checkip.length;i++){
        perm[i] = {"permission": $checkip.eq(i).val()};
    }

    $(".add_subvendor_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "first_name": $('#add_sub_name').val(),
        "username": $('#add_sub_phno').val(),
        "email": $('#add_sub_email').val(),
        "password": $('#add_sub_pwd').val(),
        "userprofile": {
            "role": 4
        },
        "permissions": perm
    });

    $.ajax({
        url: create_subvendor_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $('#add_sub_name, #add_sub_phno, #add_sub_email, #add_sub_pwd').val('');
            $(".add_subvendor_Btn").html(`Submit`).attr("disabled", false);
            $("#addsubadmins").modal("toggle");
            listdata(1);
            $("#snackbarsuccs").text("Sub Vendor Has Been Created Successfully!");
            showsuccesstoast();
            $checkip.prop('checked', true);                        
        },
        error: function(data) {
            console.log(data);
            $(".add_subvendor_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here
}


function listdata(type) {
    sessionStorage.type = type;
    var search = sessionStorage.search ? sessionStorage.search : "";
    var fromdate = sessionStorage.fromdate ? sessionStorage.fromdate.replace(/ /g, '') : "";
    var todate = sessionStorage.todate ? sessionStorage.todate.replace(/ /g, '') : "";

    var senddata = JSON.stringify({
        "search": search,
        "start_date": fromdate,
        "end_date": todate
    });

    if (type == 1) {
        var url = list_subvendors_api;
        sno = 0;
    } else {
        var url = list_subvendors_api + "?page=" + sessionStorage.gotopage_no;
        sno = 0;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: senddata,
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_listing").empty();
            $(".dyndataTable").show();
            $(".nodatarow").hide();
            sessionStorage.bookingdata = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_listing").empty();
            $(".nodatarow").show();
            $(".dyndataTable").hide();
            $(".showallBtn").empty().append(`SHOW ALL`);
            $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
var datacount = 0;

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.bookingdata);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        $(".nodatarow").show();
        $(".dyndataTable").hide();
    } else {
        if (sessionStorage.type == 1) {
            datacount = data.page_size;
            pagination(data.count);
        }
        for (var i = 0; i < data['results'].length; i++) {

            var created_date = monthNames[parseInt(data.results[i].date_joined.substring(5, 7)) - 1] + " " + data.results[i].date_joined.substring(8, 10) + ", " + data.results[i].date_joined.substring(0, 4);
            var created_time = data.results[i].date_joined.slice(11, 16);
            if (Number(created_time.slice(0, 2)) == 12) {
                created_time = created_time + ' PM';
            } else if (Number(created_time.slice(0, 2)) > 12) {
                created_time = Number(created_time.slice(0, 2)) - 12 + created_time.slice(2, 5) + ' PM';
            } else {
                created_time = created_time + ' AM';
            }
            if (data.last_login) {
                var lastlogin_date = monthNames[parseInt(data.results[i].last_login.substring(5, 7)) - 1] + " " + data.results[i].last_login.substring(8, 10) + ", " + data.results[i].last_login.substring(0, 4);
                var lastlogin_time = data.results[i].last_login.slice(11, 16);
                if (Number(lastlogin_time.slice(0, 2)) == 12) {
                    lastlogin_time = lastlogin_time + ' PM';
                } else if (Number(lastlogin_time.slice(0, 2)) > 12) {
                    lastlogin_time = Number(lastlogin_time.slice(0, 2)) - 12 + lastlogin_time.slice(2, 5) + ' PM';
                } else {
                    lastlogin_time = lastlogin_time + ' AM';
                }
            } else {
                var lastlogin_date = "Date & Time Not Mentioned";
                var lastlogin_time = "";
            }


            $(".dyn_listing").append(`<tr>
                                    <td class="text-center">${data['results'][i].id}</td>
                                    <td>${data['results'][i].first_name}</td>
                                    <td>${data['results'][i].username}</td>
                                    <td><a class="viewanchor cptr" onclick="listpermissions(${data['results'][i].id})">Click Here</a></td>
                                    <td>
                                        <a onclick="editpassword_sv(${data['results'][i].id})" class="viewanchor cptr" data-toggle="modal" data-target="#editpwdmodal">Edit Password</a>
                                    </td>
                                    <td>${lastlogin_date}
                                        <br/><span class="text-muted">${lastlogin_time}</span></td>
                                    }
                                    <td>${created_date}
                                        <br/><span class="text-muted">${created_time}</span></td>
                                    <td>
                                        <input type="checkbox" ${(data['results'][i].is_active ? "checked" : "")} class="js-switch clickcb${data['results'][i].id}" data-color="#f96262" data-size="small" onchange="dostatuschange(${data['results'][i].id})" />
                                    </td>
                                </tr>`);
        } //for loop ends here
    } //else cond ends here
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });
    $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    $(".showallBtn").empty().append(`SHOW ALL`);
}

//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / datacount) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / datacount);
    } else {
        var totalpagescount = parseInt((count / datacount), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    if ((currentpageno % 3) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
        $(".pageli").hide();
        for (var i = currentpageno; i <= currentpageno + 2; i++) {
            $(".pageli" + i).show();
        }
    }
    currentpageno++;
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

        if ((currentpageno % 3) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 2; i++) {
                $(".pageli" + i).show();
            }
        }
    } //if cond ends here
}

function previouspage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    if ((currentpageno % 3) == 0) {
        $(".pageli").hide();
        for (var i = currentpageno; i >= currentpageno - 2; i--) {
            $(".pageli" + i).show();
        }
    }
    currentpageno--;
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}

//active / Inactive fn
var vendorid = '';

function dostatuschange(id) {
    vendorid = id;
    $('#conformModal').modal({ backdrop: 'static', keyboard: false });
}

$(".closeclicked").click(function() {
    $(".clickcb" + vendorid).click();
});

function statuschange_final() {

    if ($('#changepass').val() == '') {
        $('#changepass').addClass('iserr');
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".cp_Btn").html(`Submit &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = JSON.stringify({
        "password": $("#changepass").val()
    });

    $.ajax({
        url: accrdec_user_api + vendorid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            $("#conformModal").modal("toggle");
            $("#snackbarsuccs").text("Status has been changed successfully!");
            showsuccesstoast();
            // $(".clickcb" + vendorid).click();
        },
        error: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    });
} //active / Inactive fn

function editpassword_sv(id) {
    sessionStorage.editpwd_id = id;
}

function update_sv_password() {
    if ($('#sv_newpwd').val().trim() == '') {
        $("#sv_newpwd").addClass("iserr");
        $("#snackbarerror").text("New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#sv_newpwd').val().length < 6) {
        $("#sv_newpwd").addClass("iserr");
        $("#snackbarerror").text("Atleast 6 characters is required for New password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#sv_renewpass').val().trim() == '') {
        $("#sv_renewpass").addClass("iserr");
        $("#snackbarerror").text("Re - New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#sv_renewpass').val().length < 6) {
        $("#sv_renewpass").addClass("iserr");
        $("#snackbarerror").text("Atleast 6 characters is required for re - new password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#sv_newpwd').val() != $('#sv_renewpass').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $(".updatepwdBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    var postData = JSON.stringify({
        "password": $('#sv_newpwd').val()
    });
    $.ajax({
        url: update_userpassword_api + sessionStorage.editpwd_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".updatepwdBtn").html(`Save`).attr("disabled", false);
            $("#editpwdmodal").modal("toggle");
            $("#snackbarsuccs").text("Passwords Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".updatepwdBtn").html(`Save`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    }); //done fn ends here
}

//saerch fn starts here
function searchnow() {
    sessionStorage.search = $("#searchtext").val();
    $(".searchBtn").empty().append(`<i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

function showall() {
    sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    $("#searchtext,.searchbydate").val("");
    $(".showallBtn").empty().append(`SHOW ALL &nbsp; <i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

$(".applyBtn").click(function() {
    $(".searchbydate").change(function() {
        var startdate = $("#searchdate").val().split('-')[0];
        var enddate = $("#searchdate").val().split('-')[1];
        sessionStorage.fromdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
        sessionStorage.todate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
        listdata(1);
    });
});

//list permissions
function listpermissions(id){
    var $checkip = $("#allpermissionsmodal input");
    $checkip.prop('checked', false);
    $.ajax({
        url: listpermissions_api + id + '/',
        type: 'GET',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data){
            for(var i =0; i < data.length; i++){
                $checkip.eq(i).prop("checked", data[i].is_active);
            }
            $("#allpermissionsmodal").modal('show');
            $(".assignbtn").attr("onclick", `assignPermissions(${id})`);
        },
        error: function(data){
            ajaxerrmsg(data);
        }
    })
}


//update permission function starts here
function assignPermissions(id) {
    $('.assignldr').show();
    var perm = [];
    var $checkip = $("#allpermissionsmodal input:checked");
    for(var i = 0; i< $checkip.length ; i++){
        perm[i] = $checkip.eq(i).val();
    }
    $.ajax({
        url: updatepermissions_api + id + '/',
        type: 'put',
        data: JSON.stringify({
            "permissions" : perm
        }),
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $("#allpermissionsmodal input").prop('checked', false);
            $('.assignldr').hide();
            $('.assgnperclose').click();
            $("#snackbarsuccs").text("User permission updated");
            showsuccesstoast();
        },
        error: function(data) {
            $('.assignldr').hide();
            $('.assgnperclose').click();
            ajaxerrmsg(data);
        }
    });
}

// var pages = ["dashboard.html", "calender.html", "enquiries.html", "leads.html", "bookings.html", "add-service.html", "manage-services.html", "accounts.html"];