sessionStorage.fromdate = "";
sessionStorage.todate = "";

var userid = JSON.parse(localStorage.userdetails).id;

$(function() {
	$(".nodatarow").hide();

    listdata(1);
    loadheader();
    $("#searchdate").val("");
})

function listdata(type) {
    sessionStorage.type = type;

    var fromdate = sessionStorage.fromdate ? sessionStorage.fromdate.replace(/ /g, '') : "";
    var todate = sessionStorage.todate ? sessionStorage.todate.replace(/ /g, '') : "";

    if (type == 1) {
        var url = listdetails_api + userid + '&from_date=' + fromdate + '&to_date=' + todate;
        sno = 0;
    } else {
        var url = listdetails_api + userid + '&from_date=' + fromdate + '&to_date=' + todate + "&page=" + sessionStorage.gotopage_no;
    }

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_listing").empty();
            $(".dyndataTable").show();
            $(".nodatarow").hide();
            sessionStorage.listingdata = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_listing").empty();
            $(".nodatarow").show();
            $(".dyndataTable").hide();
            $(".showallBtn").empty().append(`SHOW ALL`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
var datacount = 0;

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.listingdata);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        $(".nodatarow").show();
        $(".dyndataTable").hide();
    } else {
        if (sessionStorage.type == 1) {
            datacount = data.page_size;
            pagination(data.count);
        }
        for (var i = 0; i < data['results'].length; i++) {

            var created_date = monthNames[parseInt(data.results[i].created_on.substring(5, 7)) - 1] + " " + data.results[i].created_on.substring(8, 10) + ", " + data.results[i].created_on.substring(0, 4);
            var created_time = data.results[i].created_on.slice(11, 16);
            if (Number(created_time.slice(0, 2)) == 12) {
                created_time = created_time + ' PM';
            } else if (Number(created_time.slice(0, 2)) > 12) {
                created_time = Number(created_time.slice(0, 2)) - 12 + created_time.slice(2, 5) + ' PM';
            } else {
                created_time = created_time + ' AM';
            }

            $(".dyn_listing").append(` <tr>
                                    <td class="text-center">${i+1}</td>
                                    <td>${created_date} @ ${created_time}</td>
                                    <td>${data.results[i].leads}</td>
                                    <td>Rs. <span>${data.results[i].amount}</span></td>
                                    <td>Rs. <span>${data.results[i].discount}</span></td>
                                    <td>
                                        ${data.results[i].description}
                                    </td>
                                </tr>`);

        } //for loop ends here
    } //else cond ends here
    $(".showallBtn").empty().append(`SHOW ALL`);
}

function loadheader() {
    $.ajax({
        url: getdeatilsmodal_api + userid + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            if (data.last_clearance_date != null) {
            	var created_date = monthNames[parseInt(data.last_clearance_date.substring(5, 7)) - 1] + " " + data.last_clearance_date.substring(8, 10) + ", " + data.last_clearance_date.substring(0, 4);
            } else {
                var created_date = "-";
            }

            var amt = (data.amount_from_prev_clearance == null) ? 'Rs.' + 0 : 'Rs.' + data.amount_from_prev_clearance;
            var earned = (data.total_earned == null) ? '0' : data.total_earned;

            $('.createddate').text(created_date);
            $('.leadscount').text(data.bookings_from_last_clearance);
            $('.amountcount').text(amt);

        },
        error: function(data) {
            ajaxerrmsg(data);
        }
    });
}

//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / datacount) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / datacount);
    } else {
        var totalpagescount = parseInt((count / datacount), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno++;
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

        if ((currentpageno % 5) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 4; i++) {
                $(".pageli" + i).show();
            }
        }
    } //if cond ends here
}

function previouspage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno--;
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        if ((currentpageno % 5) == 0) {
            $(".pageli").hide();
            for (var i = currentpageno; i >= currentpageno - 4; i--) {
                $(".pageli" + i).show();
            }
        }
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}
function showall() {
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    $(".showallBtn").empty().append(`SHOW ALL &nbsp; <i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

$(".applyBtn").click(function() {
    var startdate = $("#searchdate").val().split('-')[0];
    var enddate = $("#searchdate").val().split('-')[1];
    sessionStorage.fromdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
    sessionStorage.todate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    listdata(1);
});