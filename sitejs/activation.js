function getQueryStrings() {
    var assoc = {};
    var decode = function(s) {
        return decodeURIComponent(s.replace(/\+/g, " "));
    };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');
    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }
    return assoc;
}

var qs = getQueryStrings();
sessionStorage.useridactivate = qs["id"];
sessionStorage.tokenactivate = qs["token"];

function verifyemail() {

    $(".verifyemailBtn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "uid": sessionStorage.useridactivate,
        "token": sessionStorage.tokenactivate,
        "client": uniqueclient
    });

    $.ajax({
        url: emailverify_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".verifyemailBtn").html(`Submit &nbsp;`).attr("disabled", false);
            $("#snackbarsuccs").html(`<ol><li>Your Email Has Been Verified Successfully.</li></ol>`);
            showsuccesstoast();
            setTimeout(function() { window.location.href = 'index.html' }, 3000);
        },
        error: function(data) {
            $(".verifyemailBtn").html(`Submit &nbsp;`).attr("disabled", false);
            ajaxerrmsg(data);
        }
    });
}