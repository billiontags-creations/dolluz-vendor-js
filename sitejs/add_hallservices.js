var add_images = [];
var add_videos = [];
var uploadimagescount = 0;
var uploadvideoscount = 0;
var listevents = '';
$(function() {
    $("#locationcomplete").geocomplete({
        details: ".geo-details",
        detailsAttribute: "data-geo"
    });
  
    // sessionStorage.serv_created_id = 5;
    $(".bookinservicename").text(`${sessionStorage.service_name}`);
    $(".viewaddedimages").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
    $(".viewaddedvideos").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
    $(".progress").hide();

    //images local Upload fn starts here
    if (window.File && window.FileList && window.FileReader) {

        // addproduct images change fn
        $("#addimages").on("change", function(e) {
            var files = e.target.files;
            if (uploadimagescount == 0) {
                $(".viewaddedimages").html(``);
            }
            for (var i = 0; i < files.length; i++) {
                uploadimagescount++;
                var file = files[i];
                add_images.push(file);
                showpreview(file, uploadimagescount, 1);
            }
        });

        // Reception/Lobby Images change fn
        $("#addvideosid").on("change", function(e) {
            var files = e.target.files;
            if (uploadvideoscount == 0) {
                $(".viewaddedvideos").html(``);
            }
            for (var i = 0; i < files.length; i++) {
                uploadvideoscount++;
                var file = files[i];
                add_videos.push(file);
                showpreview(file, uploadvideoscount, 2);
            }
        });
    } else {
        alert("Your browser doesn't support to File API")
    } //fn ends here

    //load service types
    $.ajax({
        url: listservicetypes_api + sessionStorage.serv_subctgry_id,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {

            for (var i = 0; i < data.length; i++) {
                $("#li_servicetype,.events_li").append(`<option value="${data[i].id}">${data[i].name}</option>`);
                listevents += `<option value="${data[i].id}">${data[i].name}</option>`;
            }
        },
        error: function(data) {
            console.log('error occured in service types');
        }
    });

});

//ext local files append fn
function showpreview(f, i, type) {
    var reader = new FileReader();
    reader.onload = (function(file) {
        return function(e) {
            if (type == 1) { // ext images append
                $(".viewaddedimages").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 plr5 addimage" + i + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='" + e.target.result + "' class='img-responsive h85px uploadimg' title='" + file.name + "'/> </div><div class='play'> <a onclick='deletethisimage_local(" + i + ",\"" + file.name + "\"," + 1 + ")' class='bt_pointer'><img src='images/deletebtn.svg' class='w60p'/></a> </div></div></div>");
            } else {
                $(".viewaddedvideos").append("<div class='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12 mt10 plr5 videoimage" + i + "'> <div class='itemsContainer'> <div class='image image_mt10'> <img src='images/filepreview.png' class='img-responsive hw100px uploadimg' title='" + file.name + "'/><p class='centered'>" + file.name + "</p> </div><div class='play'> <a onclick='deletethisimage_local(" + i + ",\"" + file.name + "\"," + 2 + ")' class='bt_pointer'><img src='images/deletebtn.svg' class='w60p'/></a> </div></div></div>");
            }
        };
    })(f, i);
    reader.readAsDataURL(f);
}

//delete images locally fn starts here
function deletethisimage_local(i, fileName, type) {
    if (type == 1) { //ext images delete fn
        $('.addimage' + i).remove();
        for (var i = 0; i < add_images.length; i++) {
            if (add_images[i].name === fileName) {
                add_images.splice(i, 1);
                break;
            }
        }
        if (add_images.length == 0) {
            uploadimagescount = 0;
            $(".viewaddedimages").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
        }

    } else { //reception images delete
        $('.videoimage' + i).remove();
        for (var i = 0; i < add_videos.length; i++) {
            if (add_videos[i].name === fileName) {
                add_videos.splice(i, 1);
                break;
            }
        }
        if (add_videos.length == 0) {
            uploadvideoscount = 0;
            $(".viewaddedvideos").html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
        }
    }
} //delete files fn starts here

function readURL(input, type) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            if (type == 1) {
                var myElement = document.querySelector(".addbannerimage");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#coverpic").change(function() {
    readURL(this, 1);
});


var s_detailsid = ['service_name', 'service_desc', 'service_street', 'service_area', 'service_city', 'service_state', 'service_country', 'service_zipcode','service_mainphno'];

var s_detailserr = ['Service Name', 'Service Description', 'Street', 'Area', 'City', 'State', 'Country', 'Zipcode', 'Main Contact Number'];

function adddetailssave_fn() {

    for (var i = 0; i < s_detailsid.length; i++) {
        if ($('#' + s_detailsid[i] + '').val() == '') {
            $('#' + s_detailsid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + s_detailserr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    

    $(".adddetailssave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var postData = {
        "sub_category": sessionStorage.serv_subctgry_id,
        "name": $("#service_name").val(),
        "description": $("#service_desc").val(),
        "address": {
            "street": $("#service_street").val(),
            "area": $("#service_area").val(),
            "city": $("#service_city").val(),
            "state": $("#service_state").val(),
            "country": $("#service_country").val(),
            "zipcode": $("#service_zipcode").val(),
            "contact_number": $("#service_mainphno").val()
        }
    };

    $("#service_lat").val()? postData.address["latitude"] = $("#service_lat").val() : '';
    $("#service_long").val() ? postData.address["longitude"] = $("#service_long").val() : '';
    $("#service_mainland").val() ? postData.address["landmark"] = $("#service_mainland").val() : '';

    $.ajax({
        url: createservice_details_api,
        type: 'post',
        data: JSON.stringify(postData),
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".adddetailssave_btn").html(`Save`).attr("disabled", false);
            sessionStorage.serv_created_id = data.id;
            $("#snackbarsuccs").text("Services Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".adddetailssave_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here

}

function uploadcoverpic() {

    var postData = new FormData();

    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#coverpic')[0].files.length == 0) {
        $("#snackbarerror").text("Cover Picture is Required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        postData.append("cover_picture", $('#coverpic')[0].files[0]);
    }

    $(".coverpicsave_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    $.ajax({
        url: createservice_cvrpic_api + sessionStorage.serv_created_id + '/',
        type: 'put',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".coverpicsave_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Cover Picture Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".coverpicsave_Btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here

}

function saveimages() {
    var postData = new FormData();

    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if (add_images.length == 0) {
        $("#snackbarerror").text("Images is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        for (i = 0; i < add_images.length; i++) {
            postData.append("images", add_images[i]);
        }
    }
    $(".progress").show();
    $('.myprogress').css('width', '0');
    postData.append("service", sessionStorage.serv_created_id);
    $(".imagesadd_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    $.ajax({
        url: createservice_imges_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        // this part is progress bar
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.myprogress').text(percentComplete + '%');
                    $('.myprogress').css('width', percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data) {
            $(".imagesadd_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Images has been updated successfully");
            showsuccesstoast();
            $("#uploadimagesmodal").modal("toggle");
            $('#addimages').val('');
            $('.viewaddedimages').html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
            add_images = [];
            uploadimagescount = 0;
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        },
        error: function(data) {
            $(".imagesadd_Btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        }
    });
}

function savevideos() {
    var postData = new FormData();

    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if (add_videos.length == 0) {
        $("#snackbarerror").text("Videos is required");
        showerrtoast();
        event.preventDefault();
        return;
    } else {
        for (i = 0; i < add_videos.length; i++) {
            postData.append("videos", add_videos[i]);
        }
    }
    $(".progress").show();
    $('.myprogress').css('width', '0');
    postData.append("service", sessionStorage.serv_created_id);
    $(".videosadd_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    $.ajax({
        url: createservice_videos_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        // this part is progress bar
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('.myprogress').text(percentComplete + '%');
                    $('.myprogress').css('width', percentComplete + '%');
                }
            }, false);
            return xhr;
        },
        success: function(data) {
            $(".videosadd_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Videos has been updated successfully");
            showsuccesstoast();
            $("#uploadvideosmodal").modal("toggle");
            $('#addvideos').val('');
            $('.viewaddedvideos').html(`<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 plr5"><center><img src="images/filepreview.png" class="previmghght"></center></div>`);
            add_videos = [];
            uploadvideoscount = 0;
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        },
        error: function(data) {
            $(".videosadd_Btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
            $(".progress").hide();
            $('.myprogress').css('width', '0');
        }
    });
}

//services types fn add 
function add_servicestype_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#li_servicetype').val() == null) {
        $("#snackbarerror").text("Services Types is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".servicestypesave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var servicetypes = [];
    for (var i = 0; i < $('#li_servicetype').val().length; i++) {
        servicetypes.push({ "type_id": $('#li_servicetype').val()[i], "service": sessionStorage.serv_created_id });
    }

    var postData = JSON.stringify(servicetypes);

    $.ajax({
        url: createservicestypes_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".servicestypesave_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Services Types Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".servicestypesave_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here
}

var addincluamenties = 0;

function addamenitiesinclusive() {
    if ($('#inclusive_name_' + addincluamenties).val() == '') {
        $('#inclusive_name_' + addincluamenties).addClass('iserr');
        $("#snackbarerror").text("Inclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#inclusive_desc_' + addincluamenties).val() == '') {
        $('#inclusive_desc_' + addincluamenties).addClass('iserr');
        $("#snackbarerror").text("Inclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".ame_inclusive_addicon" + addincluamenties).empty().append(`<a onclick="removeamen_inclusiverow(${addincluamenties})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    addincluamenties++;
    $(".addamenities_inclusiverow").append(`<div class="form-group ameinclusivefrmgrup ameinclusivefrmgrup${addincluamenties}">
                                            <label class="col-md-12">Inclusives</label>
                                            <div class="col-md-12">
                                                <input type="email" id="inclusive_name_${addincluamenties}" name="example-email" class="form-control ipfield inclusive_name" placeholder="Inclusive Name">
                                            </div>
                                            <div class="col-md-12">
                                                <br>
                                                <textarea class="form-control inclusive_desc ipfield" id="inclusive_desc_${addincluamenties}" rows="6" placeholder="Your Information Goes Here.."></textarea>
                                            </div>
                                            <div class="col-md-12 ame_inclusive_addicon${addincluamenties}">
                                                <a onclick="addamenitiesinclusive()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                            </div>
                                        </div>`);
    $(".ipfield").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });
}

function removeamen_inclusiverow(rowno) {
    $(".ameinclusivefrmgrup" + rowno).remove();
}

var addexclisiveamenties = 0;

function addamenitiesexclusive() {
    if ($('#exclusive_name_' + addexclisiveamenties).val() == '') {
        $('#exclusive_name_' + addexclisiveamenties).addClass('iserr');
        $("#snackbarerror").text("Exclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#exclusive_desc_' + addexclisiveamenties).val() == '') {
        $('#exclusive_desc_' + addexclisiveamenties).addClass('iserr');
        $("#snackbarerror").text("Exclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".ame_exclisive_addicon" + addexclisiveamenties).empty().append(`<a onclick="removeamen_exclusiverow(${addexclisiveamenties})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    addexclisiveamenties++;
    $(".addamenities_exclusiverow").append(`<div class="form-group ameexclsivefrmgrup ameexclsivefrmgrup${addexclisiveamenties}">
                                            <label class="col-md-12">Exclusives</label>
                                            <div class="col-md-12">
                                                <input type="email" id="exclusive_name_${addexclisiveamenties}" name="example-email" class="form-control ipfield exclusive_name" placeholder="Exclusives Name">
                                            </div>
                                            <div class="col-md-12">
                                                <br>
                                                <textarea class="form-control exclusive_desc ipfield" id="exclusive_desc_${addexclisiveamenties}" rows="6" placeholder="Your Information Goes Here.."></textarea>
                                            </div>
                                            <div class="col-md-12 ame_exclisive_addicon${addexclisiveamenties}">
                                                <a onclick="addamenitiesexclusive()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                            </div>
                                        </div>`);
    $(".ipfield").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });
}

function removeamen_exclusiverow(rowno) {
    $(".ameexclsivefrmgrup" + rowno).remove();
}

function add_amenities_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#inclusive_name_' + addincluamenties).val() == '') {
        $('#inclusive_name_' + addincluamenties).addClass('iserr');
        $("#snackbarerror").text("Inclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#inclusive_desc_' + addincluamenties).val() == '') {
        $('#inclusive_desc_' + addincluamenties).addClass('iserr');
        $("#snackbarerror").text("Inclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#exclusive_name_' + addexclisiveamenties).val() == '') {
        $('#exclusive_name_' + addexclisiveamenties).addClass('iserr');
        $("#snackbarerror").text("Exclusives Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#exclusive_desc_' + addexclisiveamenties).val() == '') {
        $('#exclusive_desc_' + addexclisiveamenties).addClass('iserr');
        $("#snackbarerror").text("Exclusives Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".amenities_save_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var inclusives = [];
    for (var i = 0; i < $(".ameinclusivefrmgrup").length; i++) {
        inclusives.push({ "name": $(".inclusive_name").eq(i).val(), "information": $(".inclusive_desc").eq(i).val() });
    }

    var exclusives = [];
    for (var i = 0; i < $(".ameexclsivefrmgrup").length; i++) {
        exclusives.push({ "name": $(".exclusive_name").eq(i).val(), "information": $(".exclusive_desc").eq(i).val() });
    }

    var postData = JSON.stringify({
        "service": sessionStorage.serv_created_id,
        "inclusive": inclusives,
        "exclusive": exclusives
    });

    $.ajax({
        url: createamenities_details_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".amenities_save_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Amenities Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".amenities_save_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here

}

//add contact details
var addcontactdetailsno = 0;

function addcontactdetails() {

    if ($('#cnt_name_' + addcontactdetailsno).val() == '') {
        $('#cnt_name_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_role_' + addcontactdetailsno).val() == '') {
        $('#cnt_role_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Role is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_phno_' + addcontactdetailsno).val() == '') {
        $('#cnt_phno_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_email_' + addcontactdetailsno).val() == '') {
        $('#cnt_email_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#cnt_email_' + addcontactdetailsno).val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#cnt_email_' + addcontactdetailsno).addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".cntdetails_addicon" + addcontactdetailsno).empty().append(`<a onclick="remove_cntctdet_row(${addcontactdetailsno})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    addcontactdetailsno++;

    $(".contactdetails_row").append(`<div class="contactdetails_div contactdetails_div_${addcontactdetailsno}">
                                    <div class="col-md-6">
                                        <div class="form-material form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-12">Name</label>
                                                <div class="col-md-12">
                                                    <input type="email" id="cnt_name_${addcontactdetailsno}" name="example-email" class="form-control cnt_name ipfield" placeholder="Enter Name">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-material form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-12">Role</label>
                                                <div class="col-md-12">
                                                    <input type="email" id="cnt_role_${addcontactdetailsno}" name="example-email" class="form-control cnt_role ipfield" placeholder="Enter Role">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-material form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-12">Phone Number</label>
                                                <div class="col-md-12">
                                                    <input type="email" id="cnt_phno_${addcontactdetailsno}" name="example-email" class="form-control cnt_phno ipfield" placeholder="Enter Phone Number">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-material form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-12">E-Mail ID</label>
                                                <div class="col-md-12">
                                                    <input type="email" id="cnt_email_${addcontactdetailsno}" name="example-email" class="form-control cnt_email ipfield" placeholder="Enter E-Mail ID">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 cntdetails_addicon${addcontactdetailsno}">
                                        <a onclick="addcontactdetails()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                    </div>
                                </div>`);

    $(".ipfield").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });
    $(".cnt_phno").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function remove_cntctdet_row(rowno) {
    $(".contactdetails_div_" + rowno).remove();
}

function add_contactdetails_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }

    if ($('#cnt_name_' + addcontactdetailsno).val() == '') {
        $('#cnt_name_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_role_' + addcontactdetailsno).val() == '') {
        $('#cnt_role_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Role is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_phno_' + addcontactdetailsno).val() == '') {
        $('#cnt_phno_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#cnt_email_' + addcontactdetailsno).val() == '') {
        $('#cnt_email_' + addcontactdetailsno).addClass('iserr');
        $("#snackbarerror").text("Contact Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#cnt_email_' + addcontactdetailsno).val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#cnt_email_' + addcontactdetailsno).addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".contactdetailssave_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var contactdetails = [];
    for (var i = 0; i < $('.contactdetails_div').length; i++) {
        contactdetails.push({
            "service": sessionStorage.serv_created_id,
            "name": $(".cnt_name").eq(i).val(),
            "role": $(".cnt_role").eq(i).val(),
            "contact_number": $(".cnt_phno").eq(i).val(),
            "email": $(".cnt_email").eq(i).val()
        });
    }

    var postData = JSON.stringify(contactdetails);

    $.ajax({
        url: create_contactdetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".contactdetailssave_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Contact Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".contactdetailssave_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here

}

var listarr_ip = ['seating_capacity', 'floating_capacity', 'dining_capacity', 'indoor_car_capacity', 'outdoor_car_capacity', 'indoor_bike_capacity', 'outdoor_bike_capacity', 'ac_rooms', 'non_ac_rooms' , 'hour_6_ac_cost', 'hour_6_non_ac_cost', 'hour_12_ac_cost', 'hour_12_non_ac_cost', 'hour_24_ac_cost', 'hour_24_non_ac_cost'];

var listarr_cb = ['is_decoration', 'is_ac_in_hall', 'is_ac_in_dining', 'vessels_available', 'non_veg_allowed', 'outside_food_allowed', 'homam_setup', 'power_backup'];

var optional_ip = ['veg_breakfast_cost', 'non_veg_breakfast_cost', 'veg_lunch_cost', 'non_veg_lunch_cost', 'veg_dinner_cost', 'non_veg_dinner_cost'];

var postdata = {};

function add_halldetails_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    // postdata = {
    //     "service": 2,
    //     "seating_capacity": $(".halldetailsrow .ipfield").eq(0).val(),
    //     "floating_capacity": $(".halldetailsrow .ipfield").eq(1).val(),
    //     "dining_capacity": $(".halldetailsrow .ipfield").eq(2).val(),
    //     "indoor_car_capacity": $(".halldetailsrow .ipfield").eq(3).val(),
    //     "indoor_bike_capacity": $(".halldetailsrow .ipfield").eq(5).val(),
    //     "outdoor_car_capacity": $(".halldetailsrow .ipfield").eq(4).val(),
    //     "outdoor_bike_capacity": $(".halldetailsrow .ipfield").eq(6).val(),
    //     "ac_rooms": $(".halldetailsrow .ipfield").eq(7).val(),
    //     "non_ac_rooms": $(".halldetailsrow .ipfield").eq(8).val(),
    //     "veg_breakfast_cost": $(".halldetailsrow .ipfield").eq(9).val(),
    //     "non_veg_breakfast_cost": $(".halldetailsrow .ipfield").eq(10).val(),
    //     "veg_lunch_cost": $(".halldetailsrow .ipfield").eq(11).val(),
    //     "non_veg_lunch_cost": $(".halldetailsrow .ipfield").eq(12).val(),
    //     "veg_dinner_cost": $(".halldetailsrow .ipfield").eq(13).val(),
    //     "non_veg_dinner_cost": $(".halldetailsrow .ipfield").eq(14).val(),
    //     "hour_6_ac_cost": $(".halldetailsrow .ipfield").eq(15).val(),
    //     "hour_6_non_ac_cost": $(".halldetailsrow .ipfield").eq(16).val(),
    //     "hour_12_ac_cost": $(".halldetailsrow .ipfield").eq(17).val(),
    //     "hour_12_non_ac_cost": $(".halldetailsrow .ipfield").eq(18).val(),
    //     "hour_24_ac_cost": $(".halldetailsrow .ipfield").eq(19).val(),
    //     "hour_24_non_ac_cost": $(".halldetailsrow .ipfield").eq(20).val(),
    //     "is_decoration": $(".cbx").eq(0).is(":checked") ? true : false,
    //     "is_ac_in_hall": $(".cbx").eq(1).is(":checked") ? true : false,
    //     "is_ac_in_dining": $(".cbx").eq(2).is(":checked") ? true : false,
    //     "vessels_available": $(".cbx").eq(3).is(":checked") ? true : false,
    //     "non_veg_allowed": $(".cbx").eq(4).is(":checked") ? true : false,
    //     "outside_food_allowed": $(".cbx").eq(5).is(":checked") ? true : false,
    //     "homam_setup": $(".cbx").eq(6).is(":checked") ? true : false,
    //     "power_backup": $(".cbx").eq(7).is(":checked") ? true : false,
    // };

    // for (var i = 0; i < $(".halldetailsrow .ipfield").length; i++) {
    //     if ($(".halldetailsrow .ipfield").eq(i).val() == "") {
    //         delete postdata[listarr[i]];
    //     }
    // }
    postdata = {};
    for (var i = 0; i < listarr_ip.length; i++) {
        if ($(".halldetailsrow .ipfield").eq(i).val() != "") {
            postdata[listarr_ip[i]] = $(".halldetailsrow .ipfield").eq(i).val();
        } 
        // else{
        //     $(".halldetailsrow .ipfield").eq(i).addClass("iserr");
        //     $("#snackbarerror").text($(".halldetailsrow .ipfield").eq(i).prev().text() + " is required");
        //     showerrtoast();
        //     event.preventDefault();
        //     return;
        // }
    }
    for (var i = 0; i < optional_ip.length; i++) {
        if ($(".halldetailsrow .opipfield").eq(i).val() != "") {
            postdata[optional_ip[i]] = $(".halldetailsrow .ipfield").eq(i).val();
        }
    }
    for (var i = 0; i < listarr_cb.length; i++) {
        if ($(".halldetailsrow .cbx").eq(i).val() != "") {
            postdata[listarr_cb[i]] = $(".halldetailsrow .cbx").eq(i).is(":checked") ? true : false;
        } 
    }
    postdata.service = sessionStorage.serv_created_id;
    var postData = JSON.stringify(postdata);
    $(".halldetails_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    $.ajax({
        url: create_halldetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".halldetails_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Service Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".halldetails_Btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                var errtxt = JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0];
                $("#snackbarerror").text(key + " - " + errtxt);
            }
            showerrtoast();
        }
    }); //done fn ends here
}

//delev details fn starts here
function add_deldetails_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#inside_desc').val() == '') {
        $('#inside_desc').addClass('iserr');
        $("#snackbarerror").text("Inside City Delivery Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#outside_desc').val() == '') {
        $('#outside_desc').addClass('iserr');
        $("#snackbarerror").text("Outside City Delivery Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".add_deldetails_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    var postData = JSON.stringify({
        "service": sessionStorage.serv_created_id,
        "inside_city_available": $(".cbother10").is(":checked") ? true : false,
        "inside_city_charges": $(".cbother11").is(":checked") ? true : false,
        "outside_city_available": $(".cbother20").is(":checked") ? true : false,
        "outside_city_charges": $(".cbother21").is(":checked") ? true : false,
        "inside_city_description": $('#inside_desc').val(),
        "outside_city_description": $('#outside_desc').val()
    });
    $.ajax({
        url: create_delevrydetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_deldetails_Btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Other Delivery Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".add_deldetails_Btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                var errtxt = JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0];
                $("#snackbarerror").text(key + " - " + errtxt);
            }
            showerrtoast();
        }
    }); //done fn ends here

}


var addotherdetailsno = 0;

function addotherdetails() {
    if ($('#other_name_' + addotherdetailsno).val() == '') {
        $('#other_name_' + addotherdetailsno).addClass('iserr');
        $("#snackbarerror").text("Name of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#other_pricecombo_' + addotherdetailsno).val() == '') {
        $('#other_pricecombo_' + addotherdetailsno).addClass('iserr');
        $("#snackbarerror").text("Price of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#additem_' + addotherdetailsno).val() == '') {
        $("#snackbarerror").text("Add Items is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".addicon_otherdetails_" + addotherdetailsno).empty().append(`<a onclick="remove_otherdetails(${addotherdetailsno})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    addotherdetailsno++;

    $(".otherdetailsrow_main").append(` <div class="otherdetailsrow otherdetailsrow${addotherdetailsno}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-material form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-12">Select Event</label>
                                                    <div class="col-sm-12">
                                                        <select class="form-control form-control-line events_li events_li_${addotherdetailsno}">
                                                            ${listevents}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-material form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-12">Name of the Combo</label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="other_name_${addotherdetailsno}" name="example-email" class="form-control other_name ipfield" placeholder="Eg.Combo1 / Package1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-material form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-12">Price of the Combo</label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="other_pricecombo_${addotherdetailsno}" name="example-email" class="form-control other_pricecombo ipfield" placeholder="Price">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-material form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-12">Add Items</label>
                                                    <div class="col-md-12">
                                                        <div class="tags-default">
                                                            <input type="text" value="" id="additem_${addotherdetailsno}" data-role="tagsinput" class="additems" placeholder="Add items and press enter" /> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 addicon_otherdetails_${addotherdetailsno}">
                                            <a onclick="addotherdetails()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                        </div>
                                    </div>
                                </div>`);

    $(".ipfield").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });
    $(".other_pricecombo").keypress(function(e) {
        if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    load_js();

}

function remove_otherdetails(rowno) {
    $(".otherdetailsrow" + rowno).remove();
}

function load_js() {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'sitejs/tagsinput.js';
    head.appendChild(script);
}

function add_otherdetails_fn() {
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#other_name_' + addotherdetailsno).val() == '') {
        $('#other_name_' + addotherdetailsno).addClass('iserr');
        $("#snackbarerror").text("Name of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#other_pricecombo_' + addotherdetailsno).val() == '') {
        $('#other_pricecombo_' + addotherdetailsno).addClass('iserr');
        $("#snackbarerror").text("Price of the Combo is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#additem_' + addotherdetailsno).val() == '') {
        $("#snackbarerror").text("Add Items is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".otherdetails_save_btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);

    var events = [];
    for (var i = 0; i < $('.otherdetailsrow').length; i++) {
        var fooditems = [];
        for (var j = 0; j < $(".additems").eq(i).val().split(",").length; j++) {
            fooditems.push({ "food": $(".additems").eq(i).val().split(",")[j] })
        }
        events.push({
            "event": $(".events_li").eq(i).val(),
            "name": $(".other_name").eq(i).val(),
            "price": $(".other_pricecombo").eq(i).val(),
            "food_items": fooditems
        });
    }

    var postData = JSON.stringify({
        "service": sessionStorage.serv_created_id,
        "events": events
    });

    $.ajax({
        url: create_otherdetails_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".otherdetails_save_btn").html(`Save`).attr("disabled", false);
            $("#snackbarsuccs").text("Other Details Has Been Created Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".otherdetails_save_btn").html(`Save`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here

}

//final submit fn starts here
function finalsubmit(){
    if (!sessionStorage.serv_created_id) {
        $("#snackbarerror").text("You need to add Service Name to upload this");
        showerrtoast();
        event.preventDefault();
        return;
    }
     $(".finalsubmit_Btn").html(`Please Wait &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
      $.ajax({
        url: create_finalsubmit_api + sessionStorage.serv_created_id + '/',
        type: 'put',
        headers: {
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".finalsubmit_Btn").html(`Submit`).attr("disabled", false);
            $("#snackbarsuccs").text("Details Has Been Created Successfully!");
            showsuccesstoast();
            window.location.href = "manage-services.html";
        },
        error: function(data) {
            $(".finalsubmit_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }); //done fn ends here
}