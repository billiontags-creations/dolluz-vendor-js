$(function() {

    $(".nodatarow").hide();
    sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    listdata(1);
    sidebar();
    $("#searchdate").val("");

});

function listdata(type) {
    sessionStorage.type = type;

    var search = sessionStorage.search ? sessionStorage.search : "";
    var fromdate = sessionStorage.fromdate ? sessionStorage.fromdate.replace(/ /g, '') : "";
    var todate = sessionStorage.todate ? sessionStorage.todate.replace(/ /g, '') : "";

    var senddata = JSON.stringify({
        "status": 3,
        "search": search,
        "start_date": fromdate,
        "end_date": todate
    });

    if (type == 1) {
        var url = loaddetailsebl_api;
    } else {
        var url = loaddetailsebl_api + "?page=" + sessionStorage.gotopage_no;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: senddata,
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_listing").empty();
            $(".dyndataTable").show();
            $(".nodatarow").hide();
            sessionStorage.bookingdata = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_listing").empty();
            $(".nodatarow").show();
            $(".dyndataTable").hide();
            $(".showallBtn").empty().append(`SHOW ALL`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
var datacount = 0;

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.bookingdata);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        $(".nodatarow").show();
        $(".dyndataTable").hide();
    } else {
        if (sessionStorage.type == 1) {
            datacount = data.page_size;
            pagination(data.count);
        }
        for (var i = 0; i < data['results'].length; i++) {

            var created_date = monthNames[parseInt(data.results[i].created_on.substring(5, 7)) - 1] + " " + data.results[i].created_on.substring(8, 10) + ", " + data.results[i].created_on.substring(0, 4);
            var created_time = data.results[i].created_on.slice(11, 16);
            if (Number(created_time.slice(0, 2)) == 12) {
                created_time = created_time + ' PM';
            } else if (Number(created_time.slice(0, 2)) > 12) {
                created_time = Number(created_time.slice(0, 2)) - 12 + created_time.slice(2, 5) + ' PM';
            } else {
                created_time = created_time + ' AM';
            }
            if (data.results[i].address == null) {
                var area_name = "Not Mentioned";
            } else {
                var area_name = data.results[i].address.area.name + "," + data.results[i].address.city.name;
            }

            $(".dyn_listing").append(` <tr>
                                    <td class="text-center">${i+1}</td>
                                    <td>${data.results[i].user.first_name}
                                        <br/><span class="text-muted">${area_name}</span></td>
                                    <td>${data.results[i].user.email}
                                        <br/><span class="text-muted">${data.results[i].user.username}</span></td>
                                    <td>${created_date}
                                        <br/><span class="text-muted">${created_time}</span>
                                    </td>
                                    <td>${data.results[i].service.name}</td>
                                   <td>${data.results[i].amount}</td>
                                </tr>`);
        } //for loop ends here
    } //else cond ends here
    $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    $(".showallBtn").empty().append(`SHOW ALL`);
}


//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / datacount) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / datacount);
    } else {
        var totalpagescount = parseInt((count / datacount), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno++;
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

        if ((currentpageno % 5) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 4; i++) {
                $(".pageli" + i).show();
            }
        }
    } //if cond ends here
}

function previouspage() {
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    currentpageno--;
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        if ((currentpageno % 5) == 0) {
            $(".pageli").hide();
            for (var i = currentpageno; i >= currentpageno - 4; i--) {
                $(".pageli" + i).show();
            }
        }
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}

//function to load side bar starts here
function sidebar() {

    $.ajax({
        url: loadstatistics_api + '3',
        type: 'get',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            var daily = parseInt((data.total == 0) ? 0 : (data.daily / data.total) * 100);
            var weekly = parseInt((data.total == 0) ? 0 : (data.weekly / data.total) * 100);
            var monthly = parseInt((data.total == 0) ? 0 : (data.monthly / data.total) * 100);

            $('.amt0').text(data.daily);
            $('.widthclass0').css("width", daily + "%");
            $('.textclass0').text(daily + "%");

            $('.amt1').text(data.weekly);
            $('.widthclass1').css("width", weekly + "%");
            $('.textclass1').text(weekly + "%");

            $('.amt2').text(data.monthly);
            $('.widthclass2').css("width", monthly + "%");
            $('.textclass2').text(monthly + "%");
        },
        error: function(data) {
            ajaxerrmsg(data);
        }
    });

}

//saerch fn starts here
function searchnow() {
    sessionStorage.search = $("#searchtext").val();
    $(".searchBtn").empty().append(`<i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

function showall() {
    sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    $("#searchtext,.searchbydate").val("");
    $(".showallBtn").empty().append(`SHOW ALL &nbsp; <i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

$(".applyBtn").click(function() {
    $(".searchbydate").change(function() {
        var startdate = $("#searchdate").val().split('-')[0];
        var enddate = $("#searchdate").val().split('-')[1];
        sessionStorage.fromdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
        sessionStorage.todate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
        listdata(1);
    });
});